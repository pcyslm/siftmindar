import {refineEstimate as refineEstimateExp} from "./refine-estimate-experiment.js";
import {_getJ_U_S, refineEstimate as refineEstimate} from "./refine-estimate.js";
import {estimate} from "./estimate.js";
import {screenToMarkerCoordinate, buildModelViewProjectionTransform, applyModelViewProjectionTransform, computeScreenCoordiate,} from "./utils.js";

function testRefineEstimateFunction3(){
  // 投影矩陣（3x3），根據你的相機內參矩陣
  const projectionTransform = [
    [501.4055, 0, 320.024],
    [0, 502.0557, 235.7605],
    [0, 0,     1]
  ];
  
  // 世界座標 (3D 坐標) 的例子
  const worldCoords = [
    { x: -0.075, y: 0.075, z: 0 },
    { x: 0.075, y: 0.075, z: 0 },
    { x: 0.075, y: -0.075, z: 0 },
    { x: -0.075, y: -0.075, z: 0 }
  ];
  
  // 屏幕座標 (2D 坐標) 的例子
  const screenCoords = [
    { x: 895.77, y: 1764.52 },
    { x: 748.80, y: 1743.55 },
    { x: 868.77, y: 938.77 },
    { x: 414.15, y: 1861.00 }
  ];

  const initialEstimateTransform = estimate({
    screenCoords,
    worldCoords,
    projectionTransform
  });

  console.log('initialEstimateTransform3:', initialEstimateTransform);

  // 調用 refineEstimate 函數進行精確估算
  //const refinedTransform = refineEstimateExp({
  //      initialModelViewTransform: initialEstimateTransform, 
  //      projectionTransform: projectionTransform,
  //      worldCoords: worldCoords,
  //      screenCoords: screenCoords
  //    });
  //console.log('Refined Model View Transform3:', refinedTransform);

  const refinedTransform2 = refineEstimate({
    initialModelViewTransform: initialEstimateTransform, 
    projectionTransform: projectionTransform,
    worldCoords: worldCoords,
    screenCoords: screenCoords
  });
  console.log('Refined Model View Transform3:', refinedTransform2);

}

function testRefineEstimateFunction2(){
  // 投影矩陣（3x3），根據你的相機內參矩陣
  const projectionTransform = [
    [600, 0, 320],
    [0, 600, 240],
    [0, 0,     1]
  ];
  
  // 世界座標 (3D 坐標) 的例子
  const worldCoords = [
    { x: 0, y: 0, z: 0 },
    { x: 2, y: 0, z: 0 },
    { x: 0, y: 2, z: 0 },
    { x: 2, y: 2, z: 0 }
  ];
  
  // 屏幕座標 (2D 坐標) 的例子
  const screenCoords = [
    { x: 150, y: 150 },
    { x: 300, y: 150 },
    { x: 150, y: 300 },
    { x: 300, y: 300 }
  ];

  const initialEstimateTransform = estimate({
    screenCoords,
    worldCoords,
    projectionTransform
  });

  console.log('initialEstimateTransform2:', initialEstimateTransform);

  // 調用 refineEstimate 函數進行精確估算
  const refinedTransform = refineEstimateExp({
        initialModelViewTransform: initialEstimateTransform, 
        projectionTransform: projectionTransform,
        worldCoords: worldCoords,
        screenCoords: screenCoords
      });
  console.log('Refined Model View Transform2:', refinedTransform);

  const refinedTransform2 = refineEstimate({
    initialModelViewTransform: initialEstimateTransform, 
    projectionTransform: projectionTransform,
    worldCoords: worldCoords,
    screenCoords: screenCoords
  });
  console.log('Refined Model View Transform2:', refinedTransform2);




}

function testRefineEstimateFunction5(){
  // 投影矩陣（3x3），根據你的相機內參矩陣
  const projectionTransform = [
    [610.32366943, 0, 313.3859558],
    [0, 610.5026245, 237.2507269],
    [0, 0, 1]
];

// 世界坐标 (3D 坐标) 的例子
const worldCoords = [
    { x: -0.025, y: -0.025, z: 0 },
    { x: 0.025, y: -0.025, z: 0 },
    { x: 0.025, y: 0.025, z: 0 },
    { x: -0.025, y: 0.025, z: 0 }
];

// 屏幕坐标 (2D 坐标) 的例子
const screenCoords = [
    { x: 608, y: 167 },
    { x: 514, y: 167 },
    { x: 518, y: 69 },
    { x: 611, y: 71 }
];

  const initialEstimateTransform = estimate({
    screenCoords,
    worldCoords,
    projectionTransform
  });

  console.log('initialEstimateTransform5:', initialEstimateTransform);

  // 調用 refineEstimate 函數進行精確估算
  /*
  const refinedTransform = refineEstimateExp({
        initialModelViewTransform: initialEstimateTransform, 
        projectionTransform: projectionTransform,
        worldCoords: worldCoords,
        screenCoords: screenCoords
      });
  console.log('Refined Model View Transform5:', refinedTransform);
  */

  const refinedTransform2 = refineEstimate({
    initialModelViewTransform: initialEstimateTransform, 
    projectionTransform: projectionTransform,
    worldCoords: worldCoords,
    screenCoords: screenCoords
  });
  console.log('Refined Model View Transform5:', refinedTransform2);

}

function testRefineEstimateFunction4(){
  // 投影矩陣（3x3），根據你的相機內參矩陣
  const projectionTransform = [
    [500, 0, 320],
    [0, 500, 235],
    [0, 0,     1]
  ];
  
  // 世界座標 (3D 坐標) 的例子
  const worldCoords = [
    { x: 0.0, y: 0.0, z: 0 },
    { x: 0.0, y: -330.0, z: -65.0 },
    { x: -225.0, y: 170.0, z: -135.0},
    { x: 225.0, y: 170.0, z: -135.0 },
    { x: -150.0, y: -150.0, z: -125.0 },
    { x: 150.0, y: -150.0, z: -125.0 },
  ];
  
  // 屏幕座標 (2D 坐標) 的例子
  const screenCoords = [
    { x: 196, y: 141 },
    { x: 190, y: 202 },
    { x: 196, y: 124 },
    { x: 236, y: 128 },
    { x: 186, y: 175 },
    { x: 214, y: 177 },
  ];

  const initialEstimateTransform = estimate({
    screenCoords,
    worldCoords,
    projectionTransform
  });

  console.log('initialEstimateTransform4:', initialEstimateTransform);

  // 調用 refineEstimate 函數進行精確估算
  /*
  const refinedTransform = refineEstimateExp({
        initialModelViewTransform: initialEstimateTransform, 
        projectionTransform: projectionTransform,
        worldCoords: worldCoords,
        screenCoords: screenCoords
      });
  console.log('Refined Model View Transform4:', refinedTransform);
  */

  const refinedTransform2 = refineEstimate({
    initialModelViewTransform: initialEstimateTransform, 
    projectionTransform: projectionTransform,
    worldCoords: worldCoords,
    screenCoords: screenCoords
  });
  console.log('Refined Model View Transform4:', refinedTransform2);

}

function testRefineEstimateFunction(){
  // 投影矩陣（3x3），根據你的相機內參矩陣
  const projectionTransform = [
    [500, 0, 320],
    [0, 500, 240],
    [0, 0,     1]
  ];
  
  // 世界座標 (3D 坐標) 的例子
  const worldCoords = [
    { x: 0, y: 0, z: 0 },
    { x: 1, y: 0, z: 0 },
    { x: 0, y: 1, z: 0 },
    { x: 1, y: 1, z: 0 }
  ];
  
  // 屏幕座標 (2D 坐標) 的例子
  const screenCoords = [
    { x: 100, y: 100 },
    { x: 200, y: 100 },
    { x: 100, y: 200 },
    { x: 200, y: 200 }
  ];

  const initialEstimateTransform = estimate({
    screenCoords,
    worldCoords,
    projectionTransform
  });

  console.log('initialEstimateTransform:', initialEstimateTransform);

  const refinedTransform2 = refineEstimate({
    initialModelViewTransform: initialEstimateTransform, 
    projectionTransform: projectionTransform,
    worldCoords: worldCoords,
    screenCoords: screenCoords
  });
  console.log('Refined Model View Transform:', refinedTransform2);

  /*

    // 調用 refineEstimate 函數進行精確估算
  const refinedTransform = refineEstimateExp({
        initialModelViewTransform: initialEstimateTransform, 
        projectionTransform: projectionTransform,
        worldCoords: worldCoords,
        screenCoords: screenCoords
      });
  console.log('Refined Model View Transform:', refinedTransform);

  */
  

}

function testbuildModelViewProjectionTransform1(){
  const projectionTransform = [
    [500, 0, 320],
    [0, 500, 240],
    [0, 0, 1]
  ];
  
  // 定義模型視圖矩陣
  const modelViewTransform = [
    [0.866, -0.5, 0.0, 1.0],
    [0.5, 0.866, 0.0, 2.0],
    [0.0, 0.0, 1.0, 3.0],
    [0.0, 0.0, 0.0, 1.0]
  ];

  console.log("buildModelViewProjectionTransform\n");
  const mvp = buildModelViewProjectionTransform(projectionTransform, modelViewTransform);

  // 輸出結果
  console.log("投影矩陣:");
  console.log(projectionTransform);
  console.log("\n模型視圖矩陣:");
  console.log(modelViewTransform);
  console.log("\n模型視圖投影矩陣:");
  console.log(mvp);
}

function testapplyModelViewProjectionTransform1(){
  // 創建一個示例模型視圖投影矩陣
const mvp = [
  [2, 0, 0, 0],
  [0, 2, 0, -1],
  [0, 0, 1, -2],
];

// 測試用例1：變換一個3D點
const point1 = {x: 1, y: 2, z: 3};
console.log("原始點1:", point1);
const transformedPoint1 = applyModelViewProjectionTransform(mvp, point1.x, point1.y, point1.z);
console.log("變換後的點1:", transformedPoint1);
console.log();

// 測試用例2：變換另一個3D點
const point2 = {x: 2, y: 3, z: 4};
console.log("原始點2:", point2);
const transformedPoint2 = applyModelViewProjectionTransform(mvp, point2.x, point2.y, point2.z);
console.log("變換後的點2:", transformedPoint2);
console.log();

// 測試用例3：變換原點
const origin = {x: 0, y: 0, z: 0};
console.log("原點:", origin);
const transformedOrigin = applyModelViewProjectionTransform(mvp, origin.x, origin.y, origin.z);
console.log("變換後的原點:", transformedOrigin);
}

function testcomputeScreenCoordinate1(){
  const mvp = [
    [2, 0, 0, 0],
    [0, 2, 0, -1],
    [0, 0, 1, -2]
  ];
  
  // 測試用例1：普通的3D點
  const point1 = { x: 1, y: 2, z: 3 };
  console.log("原始點1:", point1);
  const screenCoord1 = computeScreenCoordiate(mvp, point1.x, point1.y, point1.z);
  console.log("屏幕坐標1:", screenCoord1);
  console.log();
  
  // 測試用例2：Z接近於0的點
  const point2 = { x: 2, y: 3, z: 0.0001 };
  console.log("原始點2:", point2);
  const screenCoord2 = computeScreenCoordiate(mvp, point2.x, point2.y, point2.z);
  console.log("屏幕坐標2:", screenCoord2);
  console.log();
  
  // 測試用例3：Z為0的點（應該返回零向量）
  const point3 = { x: 4, y: 5, z: 0 };
  console.log("原始點3:", point3);
  const screenCoord3 = computeScreenCoordiate(mvp, point3.x, point3.y, point3.z);
  console.log("屏幕坐標3:", screenCoord3);
}

function testcomputeScreenCoordinate1(){
// 創建一個示例模型視圖投影矩陣
const mvp = [
  [2, 0, 0, 0],
  [0, 2, 0, -1],
  [0, 0, 1, -2]
];

console.log("Model View Projection Matrix:");
mvp.forEach(row => console.log(row));
console.log();

// 測試用例1：普通的屏幕坐標
const sx1 = 100, sy1 = 200;
console.log(`測試用例1 - 屏幕坐標: (${sx1}, ${sy1})`);
const markerCoord1 = screenToMarkerCoordinate(mvp, sx1, sy1);
console.log(`標記坐標1: (${markerCoord1.x}, ${markerCoord1.y})`);
console.log();

// 測試用例2：屏幕坐標接近原點
const sx2 = 1, sy2 = 1;
console.log(`測試用例2 - 屏幕坐標: (${sx2}, ${sy2})`);
const markerCoord2 = screenToMarkerCoordinate(mvp, sx2, sy2);
console.log(`標記坐標2: (${markerCoord2.x}, ${markerCoord2.y})`);
console.log();

// 測試用例3：負的屏幕坐標
const sx3 = -50, sy3 = -100;
console.log(`測試用例3 - 屏幕坐標: (${sx3}, ${sy3})`);
const markerCoord3 = screenToMarkerCoordinate(mvp, sx3, sy3);
console.log(`標記坐標3: (${markerCoord3.x}, ${markerCoord3.y})`);
}

function testGetJUS(){
  // Test matrices and vector setup (similar to C++ code)
const modelViewProjectionTransform = [
  [1, 0, 0, 1],
  [0, 1, 0, 1],
  [0, 0, 1, 1],
  [0, 0, 0, 1],
];

const modelViewTransform = [
  [1, 0, 0, 1],
  [0, 1, 0, 1],
  [0, 0, 1, 1],
  [0, 0, 0, 1],
];

const projectionTransform = [
  [1, 0, 0],
  [0, 1, 0],
  [0, 0, 1],
];

const worldCoord = { x: 1.0, y: 2.0, z: 3.0 };

// Call the function with test inputs
const result = _getJ_U_S({ modelViewProjectionTransform, modelViewTransform, projectionTransform, worldCoord });

// Print the result
console.log('J_U_S Matrix:', result);
}

function testEstimationFunction(){
  //testRefineEstimateFunction();
  //testRefineEstimateFunction2();
  //testRefineEstimateFunction5();
  //testRefineEstimateFunction3();
  testRefineEstimateFunction4();
  //testbuildModelViewProjectionTransform1();
  //testapplyModelViewProjectionTransform1();
  //testcomputeScreenCoordinate1();
  //testEstimateFunction();
  //testGetJUS();
    
  /*  
  const initialModelViewTransform = [
    [1, 0, 0, -2.19],
    [0, 1, 0, -1.39],
    [0, 0,   1, 5.00]
  ];

  const diffModelViewTransform = [[],[],[]];
  console.log("diffModelViewTransform0 ", JSON.parse(JSON.stringify(diffModelViewTransform)));
  console.log("diffModelViewTransform0 ", diffModelViewTransform);
  for (let j = 0; j < 3; j++) {
    for (let i = 0; i < 3; i++) {
      diffModelViewTransform[j][i] = initialModelViewTransform[j][i];
    }
  }
  const dx = 0.5;
  const dy = 0.5;
  console.log("diffModelViewTransform1 ", diffModelViewTransform);
  console.log("diffModelViewTransform1 ", JSON.parse(JSON.stringify(diffModelViewTransform)));
  diffModelViewTransform[0][3] = initialModelViewTransform[0][0] * dx + initialModelViewTransform[0][1] * dy + initialModelViewTransform[0][3];
  diffModelViewTransform[1][3] = initialModelViewTransform[1][0] * dx + initialModelViewTransform[1][1] * dy + initialModelViewTransform[1][3];
  diffModelViewTransform[2][3] = initialModelViewTransform[2][0] * dx + initialModelViewTransform[2][1] * dy + initialModelViewTransform[2][3];
  console.log("diffModelViewTransform2 ", diffModelViewTransform);
  console.log("diffModelViewTransform2 ", JSON.parse(JSON.stringify(diffModelViewTransform)));
  */
}


export {
    testEstimationFunction,
  }