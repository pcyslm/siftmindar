import {createRandomizer} from "./randomizer.js"
import {linePointSide,matrixInverse33,matrixMul33,quadrilateralConvex,smallestTriangleArea,multiplyPointHomographyInhomogenous,checkThreePointsConsistent,checkFourPointsConsistent,determinant} from "./geometry.js"
import {solveHomography, _denormalizeHomography, _normalizePoints} from "./homography.js"

function testCreateRandomizer1() {
    // 创建 Randomizer 实例
    const randomizer = createRandomizer();
    // 测试 arrayShuffle
    const testArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    const sampleSize = testArray.length;
    console.log('原始数组:', testArray);
    randomizer.arrayShuffle({ arr: testArray, sampleSize });
    
    console.log('洗牌后的数组:', testArray);
    // 测试 nextInt
    console.log('生成的随机数:');
    for (let i = 0; i < 5; i++) {
        console.log(randomizer.nextInt(100));
    }
}

function testGeometry(){
    const A1 = [1.0, 2.0];
    const A2 = [3.0, 4.0];
    const A3 = [5.0, 6.0];

    console.log("linePointSide1 ", linePointSide(A1, A2, A3));

    const B1 = [8.0, 2.0];
    const B2 = [5.0, 4.0];
    const B3 = [2.0, 1.0];

    console.log("linePointSide2 ", linePointSide(B1, B2, B3));

    const x1 = [1.0, 2.0];
    const x2 = [3.0, 4.0];
    const x3 = [5.0, 6.0];
    const x4 = [7.0, 8.0];

    const x1p = [9.0, 10.0];
    const x2p = [11.0, 12.0];
    const x3p = [13.0, 14.0];
    const x4p = [15.0, 16.0];

    const result1 = checkFourPointsConsistent(x1, x2, x3, x4, x1p, x2p, x3p, x4p);
    console.log("checkFourPointsConsistent ", result1);

    const result2 = checkThreePointsConsistent(x1, x2, x3, x1p, x2p, x3p);
    console.log("checkThreePointsConsistent ", result2);

    const data = [1, 2, 3, 7, 5, 6, 4, 8, 9];
    const result3 = determinant(data);
    console.log("determinant ", result3);

    let result4 = new Array(9).fill(0);
    result4 = matrixInverse33(data, 0.00001);
    for (let i = 0; i < 9; i++) {
        console.log("matrixInverse33 ", i, " ", result4[i]);
    }

    let result5 = new Array(9).fill(0);
    result5 = matrixMul33(data, data);
    for (let i = 0; i < 9; i++) {
        console.log("matrixMul33 ", i, " ", result5[i]);
    }

    let result6 = multiplyPointHomographyInhomogenous(x1, result5);
    console.log("multiplyPointHomographyInhomogenous ", result6);
    //for (let i = 0; i < 2; i++) {
    //    console.log("multiplyPointHomographyInhomogenous ", i, " ", result6[i]);
    //}

    const result7 = smallestTriangleArea(x1, x1p, x2, x2p);
    console.log("smallestTriangleArea ", result7);
    
    console.log("x1", x1, x1p, x2, x2p)
    const result8 = quadrilateralConvex(x1, x1p, x2, x2p);
    console.log("quadrilateralConvex ", result8);
}

function testHomography(){
    const coords1 = [[1, 2],[3, 4],[5, 6]];
    const result1 = _normalizePoints(coords1);
    console.log("_normalizePoints1 ", result1);

    const coords2 = [[1, 4],[6, 8],[2, 1]];
    const result2 = _normalizePoints(coords2);
    console.log("_normalizePoints2 ", result2);

    const nH = [0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5];
    const result3 = _denormalizeHomography(nH, result1.param, result2.param);
    console.log("_denormalizeHomography1 ", result3);
    
    const coords3 = [[0, 0],[100, 0],[50, 100]];
    const coords4 = [[10, 20],[190, 0],[60, 180]];
    const result4 = solveHomography(coords3, coords4);
    console.log("solveHomography ", result4);
    
}

function testUtilsFunction(){
    //testCreateRandomizer1();
    //testGeometry();
    testHomography();
}

export {
    testUtilsFunction,
}