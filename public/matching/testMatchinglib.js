import { compute as hammingCompute } from "./hamming-distance.js";
import {computeHomography} from './ransacHomography.js';

function testHammingDistance1() {
    const v1 = [18869576, 249519886];
    const v2 = [4294967295, 4287600398];
    console.log("testHammingDistance1 ", hammingCompute({v1, v2}));
}

function testHammingDistance2() {
  const v1 = [4278190079, 4292853663, 18869576, 249519886];
  const v2 = [4160617726, 4263452174, 4294967295, 4287600398];
  console.log("testHammingDistance2 ", hammingCompute({v1, v2}));
}


function testHammingDistance3() {
  const v1 = [4160749567, 16384, 4292853663, 18869576, 4160617726];
  const v2 = [4126800639, 32768, 4263452174, 4287600398, 4278190079];
  console.log("testHammingDistance3 ", hammingCompute({v1, v2}));
}

function testMatchingFunction(){
    //testHammingDistance1();
    //testHammingDistance2();
    //testHammingDistance3();
    //testRansacHomography();
  }

function testRansacHomography(){
  const keyframe= { width: 800, height: 600 };
  /*
  const coords3 = [[0, 0],[100, 100],[50, 100], [150, 0]];
  const coords4 = [[10, 0],[100, 100],[50, 100], [160, 0]];
  const result1 = computeHomography({srcPoints: coords3, dstPoints: coords4, keyframe: keyframe, quickMode:true});
  console.log("computeHomography1 ", result1);
  const result2 = computeHomography({srcPoints: coords3, dstPoints: coords4, keyframe: keyframe, quickMode:false});
  console.log("computeHomography2 ", result2);
  
  const coords5 = [[-20, 0],[-40, 50],[-60, 100], [-80, 180], [-150, 230]];
  const coords6 = [[-200, 0],[-220, 50],[-240, 100], [-260, 180], [-280, 230]];
  const result3 = computeHomography({srcPoints: coords5, dstPoints: coords6, keyframe: keyframe, quickMode:true});
  console.log("computeHomography3 ", result3);
  const result4 = computeHomography({srcPoints: coords5, dstPoints: coords6, keyframe: keyframe, quickMode:false});
  console.log("computeHomography4 ", result4);  
  */
  const coords7 = [[-15, 0],[-30, 25],[-90, 100], [-60, 75], [-45, 50]];
  const coords8 = [[-30, 50],[-40, 75],[-20, -25], [-60, -100], [-10, 0]];
  const result5 = computeHomography({srcPoints: coords7, dstPoints: coords8, keyframe: keyframe, quickMode:true});
  console.log("computeHomography5 ", result5);
  const result6 = computeHomography({srcPoints: coords7, dstPoints: coords8, keyframe: keyframe, quickMode:false});
  console.log("computeHomography6 ", result6);  
}



export {
  testMatchingFunction,
  
  }