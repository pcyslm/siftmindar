import {Matrix, inverse} from 'ml-matrix';
import {createRandomizer} from '../utils/randomizer.js';
import {quadrilateralConvex, matrixInverse33, smallestTriangleArea, multiplyPointHomographyInhomogenous, checkThreePointsConsistent, checkFourPointsConsistent, determinant} from '../utils/geometry.js';
import {solveHomography} from '../utils/homography.js';

const CAUCHY_SCALE = 0.01;
const CHUNK_SIZE = 10;
const NUM_HYPOTHESES = 20;//20;
const NUM_HYPOTHESES_QUICK = 10;

function coordToTxt(srcCoords, dstCoords, filename = 'matchingCoords.txt') {
  // 確保兩個陣列長度相同
  if (srcCoords.length !== dstCoords.length) {
      console.error('Screen coordinates and world coordinates arrays must have the same length');
      return;
  }

  // 組合座標數據
  let data = '';
  for (let i = 0; i < srcCoords.length; i++) {
      // 每行包含螢幕座標和世界座標
      data += `${srcCoords[i][0]}, ${srcCoords[i][1]}, ${dstCoords[i][0]}, ${dstCoords[i][1]}\n`;
  }

  try {
      // 創建 Blob 物件
      const blob = new Blob([data], { type: 'text/plain' });

      // 處理不同瀏覽器的下載方式
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          // For IE
          window.navigator.msSaveOrOpenBlob(blob, filename);
          return;
      }

      // 創建下載 URL
      const url = URL.createObjectURL(blob);

      // 創建並觸發下載連結
      const link = document.createElement('a');
      link.href = url;
      link.download = filename;
      link.style.display = 'none';
      
      // 確保連結被加到 DOM 中
      document.body.appendChild(link);
      
      // 觸發下載
      link.click();
      
      // 清理
      setTimeout(() => {
          document.body.removeChild(link);
          URL.revokeObjectURL(url);
      }, 100);

  } catch (error) {
      console.error('Error creating file:', error);
  }
}

// Using RANSAC to estimate homography
const computeHomography = (options) => {
  const {srcPoints, dstPoints, keyframe, quickMode} = options;
  coordToTxt(srcPoints, dstPoints);
  console.log("computeHomography input ", srcPoints, dstPoints);
  // testPoints is four corners of keyframe
  const testPoints = [
    [0, 0],
    [keyframe.width, 0],
    [keyframe.width, keyframe.height],
    [0, keyframe.height]
  ]
  console.log("testPoints ", testPoints);

  const sampleSize = 4; // use four points to compute homography
  //console.log("srcPoints.length ", srcPoints.length) 
  if (srcPoints.length < sampleSize) return null;

  const scale = CAUCHY_SCALE;
  const oneOverScale2 = 1.0 / (scale * scale);
  const chuckSize = Math.min(CHUNK_SIZE, srcPoints.length);
  //console.log("check scale", scale, oneOverScale2, chuckSize);

  const randomizer = createRandomizer();
  const perm = [];
  for (let i = 0; i < srcPoints.length; i++) {
    perm[i] = i;
  }
  randomizer.arrayShuffle({arr: perm, sampleSize: perm.length});
  console.log("computeHomography perm ", perm, randomizer.seed, perm.length);
  
  const numHypothesis = quickMode? NUM_HYPOTHESES_QUICK: NUM_HYPOTHESES;
  const maxTrials = numHypothesis * 2;
  //console.log("maxTrials ", maxTrials, numHypothesis);

  // build numerous hypotheses by randoming draw four points
  // TODO: optimize: if number of points is less than certain number, can brute force all combinations
  let trial = 0;
  const Hs = [];
  while (trial < maxTrials && Hs.length < numHypothesis) {
    trial +=1;

    randomizer.arrayShuffle({arr: perm, sampleSize: sampleSize});
    console.log("perm ", trial, perm, randomizer.seed, perm.length);

    // their relative positions match each other
    //console.log("checkFourPointsConsistent ", checkFourPointsConsistent(
    //  srcPoints[perm[0]], srcPoints[perm[1]], srcPoints[perm[2]], srcPoints[perm[3]],
    //  dstPoints[perm[0]], dstPoints[perm[1]], dstPoints[perm[2]], dstPoints[perm[3]]));
    console.log("2permSrcPoints ", trial, srcPoints[perm[0]], srcPoints[perm[1]], srcPoints[perm[2]], srcPoints[perm[3]]);
    console.log("2permDstPoints ", trial, dstPoints[perm[0]], dstPoints[perm[1]], dstPoints[perm[2]], dstPoints[perm[3]]);
    if (!checkFourPointsConsistent(
      srcPoints[perm[0]], srcPoints[perm[1]], srcPoints[perm[2]], srcPoints[perm[3]],
      dstPoints[perm[0]], dstPoints[perm[1]], dstPoints[perm[2]], dstPoints[perm[3]])) {
      //console.log("checkFourPointsConsistent ");
      continue;
    }

    const H = solveHomography(
      [srcPoints[perm[0]], srcPoints[perm[1]], srcPoints[perm[2]], srcPoints[perm[3]]],
      [dstPoints[perm[0]], dstPoints[perm[1]], dstPoints[perm[2]], dstPoints[perm[3]]],
    );
    if (H === null) continue;

    //console.log("_checkHomographyPointsGeometricallyConsistent H ", H, _checkHomographyPointsGeometricallyConsistent({H, testPoints}));
    if(!_checkHomographyPointsGeometricallyConsistent({H, testPoints})) {
      //console.log("_checkHomographyPointsGeometricallyConsistent ");
      continue;
    }
    
    Hs.push(H);
  }
  //testH = [5.59, 1.73, -196.151, 0.677959, 2.34382, 36.269, 0.004671, 0.0020181, 1];
  //Hs.push(testH);
  console.log("Hs", Hs);
  if (Hs.length === 0) return null;

  // pick the best hypothesis
  const hypotheses = [];
  for (let i = 0; i < Hs.length; i++) {
    hypotheses.push({
      H: Hs[i],
      cost: 0
    })
  }

  let curChuckSize = chuckSize;
  console.log("srcPoints length ", srcPoints.length);

  for (let i = 0; i < srcPoints.length && hypotheses.length > 2; i += curChuckSize) {
    curChuckSize = Math.min(chuckSize, srcPoints.length - i);
    let chuckEnd = i + curChuckSize;
    console.log("index check ", i, srcPoints.length, hypotheses.length, curChuckSize, chuckEnd);

    for (let j = 0; j < hypotheses.length; j++) {
      for (let k = i; k < chuckEnd; k++) {
        const cost = _cauchyProjectiveReprojectionCost({H: hypotheses[j].H, srcPoint: srcPoints[k], dstPoint: dstPoints[k], oneOverScale2});
        console.log("cost ", i,j, k, srcPoints[k], dstPoints[k], hypotheses[j].H, hypotheses[j].cost, cost);
        hypotheses[j].cost += cost;
      }
    }

    hypotheses.sort((h1, h2) => {return h1.cost - h2.cost});
    console.log("keepCount ", -Math.floor((hypotheses.length+1)/2), (hypotheses.length+1)/2);
    hypotheses.splice(-Math.floor((hypotheses.length+1)/2)); // keep the best half
    console.log("index check ", i, srcPoints.length, hypotheses.length, curChuckSize, chuckEnd);
  }

  console.log("hypotheses ", hypotheses);

  let finalH = null;
  for (let i = 0; i < hypotheses.length; i++) {
    //console.log("h ", hypotheses[i].H);
    const H = _normalizeHomography({inH: hypotheses[i].H});
    console.log("_checkHeuristics ", _checkHeuristics({H: H, testPoints, keyframe}));
    if (_checkHeuristics({H: H, testPoints, keyframe})) {
      finalH = H;
      break;
    }
  }
  return finalH;
}

const _checkHeuristics = ({H, testPoints, keyframe}) => {
  const HInv = matrixInverse33(H, 0.00001);
  if (HInv === null) return false;

  const mp = []
  for (let i = 0; i < testPoints.length; i++) { // 4 test points, corner of keyframe
    mp.push(multiplyPointHomographyInhomogenous(testPoints[i], HInv));
  }
  const smallArea = smallestTriangleArea(mp[0], mp[1], mp[2], mp[3]);

  if (smallArea < keyframe.width * keyframe.height * 0.0001) return false;

  if (!quadrilateralConvex(mp[0], mp[1], mp[2], mp[3])) return false;

  return true;
}

const _normalizeHomography = ({inH}) => {
  const oneOver = 1.0 / inH[8];

  const H = [];
  for (let i = 0; i < 8; i++) {
    H[i] = inH[i] * oneOver;
  }
  H[8] = 1.0;
  return H;
}

const _cauchyProjectiveReprojectionCost = ({H, srcPoint, dstPoint, oneOverScale2}) => {
  const x = multiplyPointHomographyInhomogenous(srcPoint, H);
  const f =[
    x[0] - dstPoint[0],
    x[1] - dstPoint[1]
  ];
  console.log("_cauchyProjectiveReprojectionCost x ", x);
  return Math.log(1 + (f[0]*f[0]+f[1]*f[1]) * oneOverScale2);
}

const _checkHomographyPointsGeometricallyConsistent = ({H, testPoints}) => {
  const mappedPoints = [];
  for (let i = 0; i < testPoints.length; i++) {
    
    mappedPoints[i] = multiplyPointHomographyInhomogenous(testPoints[i], H);
    //console.log("multiplyPointHomographyInhomogenous ", mappedPoints[i]);
  }
  for (let i = 0; i < testPoints.length; i++) {
    const i1 = i;
    const i2 = (i+1) % testPoints.length;
    const i3 = (i+2) % testPoints.length;
    if (!checkThreePointsConsistent(
      testPoints[i1], testPoints[i2], testPoints[i3],
      mappedPoints[i1], mappedPoints[i2], mappedPoints[i3])){
        console.log("checkThreePointsConsistent");
        return false;
      } 
  }
  return true;
}

export {
  computeHomography,
}
