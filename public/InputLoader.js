import * as tf from '@tensorflow/tfjs';
await window.tf.setBackend('webgl');
const textureMethod = window.tf.env().getNumber('WEBGL_VERSION') === 2? 'texture': 'texture2D';

export class InputLoader {
            constructor(width, height) {
                this.width = width;
                this.height = height;
                this.texShape = [height, width];

                const context = document.createElement('canvas').getContext('2d');
                context.canvas.width = width;
                context.canvas.height = height;
                this.context = context;
            }
			
			async initialize() {
				// 确保使用 CPU 后端
				await window.tf.setBackend('cpu');
				await window.tf.ready();
				console.log('TensorFlow backend:', window.tf.getBackend());
			}

            loadInput(input) {
			    //console.log("loadInput");
                this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
                this.context.drawImage(input, 0, 0, this.width, this.height);

                const backend = tf.backend();
				//console.log("backend");
                const tempPixelHandle = backend.makeTensorInfo(this.texShape, 'float32');
                //backend.gpgpu.uploadPixelDataToTexture(backend.getTexture(tempPixelHandle.dataId), this.context.canvas);

				// RGB Channel
				const originalImageTensor = tf.browser.fromPixels(this.context.canvas);
                //console.log("Original Image Tensor:");
				//originalImageTensor.print();
                //console.log("Original Image Tensor end");

                const res = this._compileAndRun(this.buildWEBGLProgram(this.width, this.height), [tempPixelHandle]);
				//console.log("res", res);
                //res.print();
                //console.log("res end");
                return res;
            }

            buildWEBGLProgram(width, height) {
			    
                const program = {
                    variableNames: ['A'],
                    outputShape: this.texShape,
                    userCode:
                        `
						void main() {
						ivec2 coords = getOutputCoords();
						int texR = coords[0];
						int texC = coords[1];
						vec2 uv = (vec2(texC, texR) + halfCR) / vec2(${width}.0, ${height}.0);
						vec4 values = ${textureMethod}(A, uv);
						setOutput((0.299 * values.r + 0.587 * values.g + 0.114 * values.b) * 255.0);
						}
						`
                };
                console.log(`Generated userCode for program: ${program.userCode}`);
                return program;
            }

            _compileAndRun(program, inputs) {
                try {
                    const outInfo = tf.backend().compileAndRun(program, inputs);
                    return tf.engine().makeTensorFromDataId(outInfo.dataId, outInfo.shape, outInfo.dtype);
                } catch (error) {
                    console.error('Error during _compileAndRun', error);
                    throw error;
                }
            }
        }
