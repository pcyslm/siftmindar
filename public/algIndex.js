import * as tf from '@tensorflow/tfjs' 
// import { CropDetector } from './crop-detector.js';
import { CropDetector } from './detector/crop-detector.js';
//import { InputLoader } from './InputLoader.js';
import { testFunction } from './detector/cpulib/testlib.js';

import './testAlg.js'
import './detector/freak.js'
import { FREAKPOINTS } from './detector/freak.js';

import {Detector} from './detector/detector.js';
import {buildImageList, buildTrackingImageList} from './image-list.js';
import { build as hierarchicalClusteringBuild } from './matching/hierarchical-clustering.js';
import { extractTrackingFeatures } from './tracker/extract-utils.js';
import { Tracker } from './tracker/tracker.js';
import { Estimator } from './estimation/estimator.js';
import { extract } from './tracker/extract.js';

import { Matcher } from './matching/Matcher.js';
import { testMatchingFunction } from './matching/testMatchinglib.js';
import { testUtilsFunction } from './utils/testUtilslib.js';
import { testEstimationFunction } from './estimation/testEstimationlib.js';
import {ovoMatching} from './matching/ovomatching.js';
tf.setBackend('cpu');
//await tf.setBackend('webgl');

console.log('start');
const fileInput = document.getElementById('fileInput');
const originalImage = document.getElementById('originalImage');
const fileInputs = document.getElementById('fileInputs');

function testFunc(){
  //console.log(FREAKPOINTS);
  //testMatchingFunction();
  //testUtilsFunction();
}

function showImage(image) {
  const points = image.points;

  // 創建畫布並設置其大小
  const canvas = document.getElementById('canvas');
  canvas.width = image.width;
  canvas.height = image.height;
  const ctx = canvas.getContext('2d');

  // 創建 ImageData 對象
  const imgData = ctx.createImageData(image.width, image.height);
  const rgbaData = new Uint8ClampedArray(image.width * image.height * 4);

  // 將灰階 image.data 填充到 ImageData 的 RGBA 格式中
  for (let i = 0; i < image.data.length; i++) {
    const value = image.data[i];
    rgbaData[i * 4] = value;        // R
    rgbaData[i * 4 + 1] = value;    // G
    rgbaData[i * 4 + 2] = value;    // B
    rgbaData[i * 4 + 3] = 255;      // A
  }

  // 將 RGBA 數據放入 ImageData
  for (let i = 0; i < rgbaData.length; i++) {
    imgData.data[i] = rgbaData[i];
  }

  // 繪製圖像到畫布
  ctx.putImageData(imgData, 0, 0);

  // 設置特徵點樣式
  ctx.fillStyle = 'red';  // 特徵點顏色
  ctx.strokeStyle = 'red';
  ctx.lineWidth = 2;

  // 繪製特徵點
  points.forEach(point => {
    ctx.beginPath();
    ctx.arc(point.x, point.y, 5, 0, 2 * Math.PI);  // 繪製圓圈
    ctx.fill();
    ctx.stroke();
  });
}

fileInput.addEventListener('change', event => {
  console.log('single fileInput change check');
  const file = event.target.files[0];
  testDetectResTXTAPI(file);

  //loadFASTFromFile(file);
  //testCropDetect(file);
  //testImageList(file);
  //testEstimationFunction();
  //testTrackingPtsAlgCase(file);
  //testTrackingPtsCase(file);
  
  //testFuncCase(file);
  //testAlgCase(file);
  //testFunc();

  //const files = event.target.files;
  //testImageMatching(files);
});

fileInputs.addEventListener('change', event => {
  console.log('multiple fileInput change');
  const files = event.target.files;
  //testImageMatching(files);
  testFASTTrackingAPI(files);

  //testTrackingCase(files);
  
});

const _extractMatchingFeatures = async (imageList, doneCallback) => {
  const keyframes = [];
  // Reuse the detector for all images
  const detector = new Detector(imageList[0].width, imageList[0].height);

  for (let i = 0; i < imageList.length; i++) {
    const image = imageList[i];

    await tf.nextFrame();
    const keyframe = tf.tidy(() => {
      const inputT = tf.tensor(image.data, [image.height, image.width], 'float32');

      // Call detector.detect and process the points
      const { featurePoints: ps } = detector.detect(inputT);

      const maximaPoints = ps.filter((p) => p.maxima);
      const minimaPoints = ps.filter((p) => !p.maxima);
      const maximaPointsCluster = hierarchicalClusteringBuild({ points: maximaPoints });
      const minimaPointsCluster = hierarchicalClusteringBuild({ points: minimaPoints });

      return {
        maximaPoints,
        minimaPoints,
        maximaPointsCluster,
        minimaPointsCluster,
        width: image.width,
        height: image.height,
        scale: image.scale,
      };
    });

    keyframes.push(keyframe);
  }

  // Call the doneCallback if provided
  if (doneCallback) {
    doneCallback(keyframes);
  }

  return keyframes;
};

function testFuncCase(file){
  if (file) {
    const reader = new FileReader();
    reader.onload = function (e) {
      console.log('reader onload');
      originalImage.src = e.target.result;
      originalImage.onload = function () {
        const width = originalImage.naturalWidth;
        const height = originalImage.naturalHeight;
        console.log('originalImage onload width, height ', width, height);

        //const loader = new InputLoader(width, height);
        //console.log(1);
        //console.log('image loader');
        //const inputT = loader.loadInput(originalImage);
        //const inputA = inputT.dataSync(); // 这是一个 TypedArray
        //console.log('inputA');
        //console.log(Array.from(inputA)); // 将 TypedArray 转换为普通数组并打印
        //console.log('inputA end');
          
        //const cropDetector = new CropDetector(width, height, false);
        //const { featurePoints, debugExtra } = cropDetector.detectMoving(inputT)
        //console.log(featurePoints);
        testFunc();
      };
    };

    reader.readAsDataURL(file);
  }
}

function loadImage(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = (e) => {
      const img = new Image();
      img.onload = () => {
        // 创建一个 canvas 以获取图像数据
        const canvas = document.createElement('canvas');
        canvas.width = img.width;
        canvas.height = img.height;
        const ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0);

        // 获取图像数据
        const processData = ctx.getImageData(0, 0, img.width, img.height);
        const greyImageData = new Uint8Array(processData.data.length / 4);

        // 转换为灰度图像数据
        for (let i = 0; i < greyImageData.length; i++) {
          const offset = i * 4;
          greyImageData[i] = Math.floor(
            (processData.data[offset] + processData.data[offset + 1] + processData.data[offset + 2]) / 3
          );
        }
        
        const width = img.width;
        const height = img.height;
        // 返回灰度图像数据
        console.log("check ", greyImageData, width, height);
        resolve({greyImageData, width, height});
      };
      img.onerror = reject;
      img.src = e.target.result;
    };
    reader.onerror = reject;
    reader.readAsDataURL(file);
  });
}

function testImageList(file){
  if (file) {
  const reader = new FileReader();
  reader.onload = function (e) {
      console.log('reader onload');
      originalImage.src = e.target.result;

      originalImage.onload = function () {
          const processCanvas = document.createElement('canvas');
          processCanvas.width = originalImage.width;
          processCanvas.height = originalImage.height;
          const processContext = processCanvas.getContext('2d');

          // Draw the image onto the canvas
          processContext.drawImage(originalImage, 0, 0, originalImage.width, originalImage.height);

          // Get image data
          const processData = processContext.getImageData(0, 0, originalImage.width, originalImage.height);

          // Create grayscale image data
          const greyImageData = new Uint8Array(originalImage.width * originalImage.height);
          console.log("processData ", processData);
          for (let i = 0; i < greyImageData.length; i++) {
              const offset = i * 4;
              greyImageData[i] = Math.floor((processData.data[offset] + processData.data[offset + 1] + processData.data[offset + 2]) / 3);
          }
          console.log("greyImageData ", greyImageData, originalImage.width, originalImage.height);

          // Create target image object
          const targetImage = {
              data: greyImageData,
              height: originalImage.height,
              width: originalImage.width
          };

          // Processed image is available in targetImage
          console.log('Processed image:', targetImage);

          
          
          imageList = buildImageList(targetImage);
          console.log("imageList", imageList);
      }
  }
  reader.readAsDataURL(file);
}
}

function testImageMatching(files){
  if (files.length !== 2) {
    console.error('請選擇兩張圖片');
    return;
  }

  const file1 = files[1];
  const file2 = files[0];

  Promise.all([
    loadImage(file1),
    loadImage(file2)
  ]).then(([{greyImageData: image1data, width: img1width, height: img1height}, {greyImageData: image2data, width: img2width, height: img2height}]) => {
    console.log("image1 ", image1data, img1width, img1height);
    console.log("image2 ", image2data, img2width, img2height);
    const Image1 = {
      data: image1data,
      height: img1height,
      width: img1width
    };
    const detector1 = new Detector(img1width, img1height);
    const Image2 = {
      data: image2data,
      height: img2height,
      width: img2width
    };
    const detector2 = new Detector(img2width, img2height);
    console.log("image ", Image1, Image2);

    tf.nextFrame();
    const keyframes = [];
    const keyframe = tf.tidy(() => {
      const inputT1 = tf.tensor(Image1.data, [img1height, img1width], 'float32');
      const {featurePoints: ps1} = detector1.detect(inputT1);

      const inputT2 = tf.tensor(Image2.data, [img2height, img2width], 'float32');
      const {featurePoints: ps2} = detector2.detect(inputT2);

      console.log("ps1 ", ps1, ps1.length);
      console.log("ps2 ", ps2, ps2.length);
      const maximaPoints1 = ps1.filter((p) => p.maxima);
      const minimaPoints1 = ps1.filter((p) => !p.maxima);
      const maximaPoints2 = ps2.filter((p) => p.maxima);
      const minimaPoints2 = ps2.filter((p) => !p.maxima);
      const maximaPointsCluster1 = hierarchicalClusteringBuild({ points: maximaPoints1 });
      const minimaPointsCluster1 = hierarchicalClusteringBuild({ points: minimaPoints1 });
      const maximaPointsCluster2 = hierarchicalClusteringBuild({ points: maximaPoints2 });
      const minimaPointsCluster2 = hierarchicalClusteringBuild({ points: minimaPoints2 });
      console.log("ps1info ", maximaPoints1, minimaPoints1);
      console.log("ps2info ", maximaPoints2, minimaPoints2);

      let ovoMatchingFlag = ovoMatching(Image1, Image2, ps1, ps2);
      console.log("ovoMatchingFlag ", ovoMatchingFlag);
      
      let screenCoords = [];
      let worldCoords = [];
      for (let i = 0; i < ovoMatchingFlag.bestMatchies.length; i++) {
           const querypoint = ovoMatchingFlag.bestMatchies[i].querypoint;
           const keypoint = ovoMatchingFlag.bestMatchies[i].keypoint;
           screenCoords.push({
            x: querypoint.x,
            y: querypoint.y,          
          })
          worldCoords.push({
          x: (keypoint.x + 0.5) / 1,
          y: (keypoint.y + 0.5) / 1,
          z: 0,
        })
      }  
      const fovy = 45.0 * Math.PI / 180; // 45 in radian. field of view vertical
      inputHeight = 720;
      inputWidth = 1280;
      const f = (inputHeight/2) / Math.tan(fovy/2);
      projectionTransform = [
        [f, 0, inputWidth / 2],
        [0, f, inputHeight / 2],
        [0, 0, 1]
        ];
      console.log("projectTransform", projectionTransform);

      estimator = new Estimator(projectionTransform);
      console.log("screenCoords ", screenCoords);
      console.log("worldCoords ", worldCoords);
      coordToTxt(screenCoords, worldCoords);
      const modelViewTransform = estimator.estimate({ screenCoords, worldCoords });
      console.log("modelViewTransform ", modelViewTransform);
      //let angle = (Math.atan2(modelViewTransform[1], modelViewTransform[0]) * 57.2957795)%360.0;
      //if (angle < 0)
      //{
      //  angle += 360;
      //}
      //console.log("angle ", angle);
       
    })



  }).catch(error => {
    console.error('Error loading images:', error);
  });
}

function coordToTxt(screenCoords, worldCoords, filename = 'coords.txt') {
  // 確保兩個陣列長度相同
  if (screenCoords.length !== worldCoords.length) {
      console.error('Screen coordinates and world coordinates arrays must have the same length');
      return;
  }

  // 組合座標數據
  let data = '';
  for (let i = 0; i < screenCoords.length; i++) {
      // 每行包含螢幕座標和世界座標
      data += `${screenCoords[i].x}, ${screenCoords[i].y}, ${worldCoords[i].x}, ${worldCoords[i].y}\n`;
  }

  try {
      // 創建 Blob 物件
      const blob = new Blob([data], { type: 'text/plain' });

      // 處理不同瀏覽器的下載方式
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          // For IE
          window.navigator.msSaveOrOpenBlob(blob, filename);
          return;
      }

      // 創建下載 URL
      const url = URL.createObjectURL(blob);

      // 創建並觸發下載連結
      const link = document.createElement('a');
      link.href = url;
      link.download = filename;
      link.style.display = 'none';
      
      // 確保連結被加到 DOM 中
      document.body.appendChild(link);
      
      // 觸發下載
      link.click();
      
      // 清理
      setTimeout(() => {
          document.body.removeChild(link);
          URL.revokeObjectURL(url);
      }, 100);

  } catch (error) {
      console.error('Error creating file:', error);
  }
}

function featurePointsToTxT(ps){
  //const data = "This is the content of the file.";

  // 創建 Blob 對象
  //const blob = new Blob([data], { type: 'text/plain' });
  const lines = ps.map(fp =>
    `${fp.maxima ? 1 : 0}, ${fp.x}, ${fp.y}, ${fp.scale}, ${fp.angle}`
  );
  const data = lines.join('\n');
  const blob = new Blob([data], { type: 'text/plain' });

  // 創建 URL 對象
  const url = URL.createObjectURL(blob);

  // 創建下載鏈接
  const a = document.createElement('a');
  a.href = url;
  a.download = 'featurePoints.txt'; // 文件名
  a.style.display = 'none'; // 隱藏鏈接
  document.body.appendChild(a);
  a.click();

  // 清理
  document.body.removeChild(a);
  URL.revokeObjectURL(url);
}

function desToTxT(ps){
  //const data = "This is the content of the file.";

  // 創建 Blob 對象
  //const blob = new Blob([data], { type: 'text/plain' });
  const lines = ps.map(fp =>
    `${fp.descriptors.join(', ')}`
  );
  const data = lines.join('\n');
  const blob = new Blob([data], { type: 'text/plain' });

  // 創建 URL 對象
  const url = URL.createObjectURL(blob);

  // 創建下載鏈接
  const a = document.createElement('a');
  a.href = url;
  a.download = 'descriptor.txt'; // 文件名
  a.style.display = 'none'; // 隱藏鏈接
  document.body.appendChild(a);
  a.click();

  // 清理
  document.body.removeChild(a);
  URL.revokeObjectURL(url);
}

function testCropDetect(file){
  if (file) {
    const reader = new FileReader();
    reader.onload = function (e) {
        console.log('reader onload');
        originalImage.src = e.target.result;

        originalImage.onload = function () {
            const processCanvas = document.createElement('canvas');
            processCanvas.width = originalImage.width;
            processCanvas.height = originalImage.height;
            const processContext = processCanvas.getContext('2d');

            // Draw the image onto the canvas
            processContext.drawImage(originalImage, 0, 0, originalImage.width, originalImage.height);

            // Get image data
            const processData = processContext.getImageData(0, 0, originalImage.width, originalImage.height);

            // Create grayscale image data
            const greyImageData = new Uint8Array(originalImage.width * originalImage.height);
            for (let i = 0; i < greyImageData.length; i++) {
                const offset = i * 4;
                greyImageData[i] = Math.floor((processData.data[offset] + processData.data[offset + 1] + processData.data[offset + 2]) / 3);
            }
            

            // Create target image object
            const targetImage = {
                data: greyImageData,
                height: originalImage.height,
                width: originalImage.width
            };
            
            // Display the processed grayscale image
            const outputCanvas = document.createElement('canvas');
            outputCanvas.width = targetImage.width;
            outputCanvas.height = targetImage.height;
            const outputContext = outputCanvas.getContext('2d');
            const outputImageData = outputContext.createImageData(targetImage.width, targetImage.height);
            for (let i = 0; i < targetImage.data.length; i++) {
                const value = targetImage.data[i];
                outputImageData.data[i * 4] = value;     // R
                outputImageData.data[i * 4 + 1] = value; // G
                outputImageData.data[i * 4 + 2] = value; // B
                outputImageData.data[i * 4 + 3] = 255;   // A
            }
            outputContext.putImageData(outputImageData, 0, 0);
            document.body.appendChild(outputCanvas);

            const cropDetector = new CropDetector(targetImage.width, targetImage.height);
            const inputT = tf.tensor(targetImage.data, [targetImage.height, targetImage.width], 'float32');
            cropDetector.detect(inputT);

            cropDetector.detectMoving(inputT);
            /*
            const detector = new Detector(targetImage.width, targetImage.height);
            tf.nextFrame();
            const keyframe = tf.tidy(() => {
              const inputT = tf.tensor(targetImage.data, [targetImage.height, targetImage.width], 'float32');
              const { featurePoints: ps } = detector.detect(inputT);
              console.log("ps ", ps);
              desToTxT(ps);
              featurePointsToTxT(ps);
              
            });

            console.log('greyImageData', greyImageData);
            console.log('processData', processData);
            //const matchingData = _extractMatchingFeatures(imageList);
            //const trackingImageList = buildTrackingImageList(imageList);
            //console.log("matchingData ", matchingData);
            //console.log("trackingImageList ", trackingImageList);
            //const trackingData = extractTrackingFeatures(imageList);
            //console.log("trackingData ", trackingData);
            //showImage(trackingData[1]);
            */
            
        }
    }
    reader.readAsDataURL(file);
  }
}

function testDetectResTXTAPI(file){
  if (file) {
    const reader = new FileReader();
    reader.onload = function (e) {
        console.log('reader onload');
        originalImage.src = e.target.result;

        originalImage.onload = function () {
            const processCanvas = document.createElement('canvas');
            processCanvas.width = originalImage.width;
            processCanvas.height = originalImage.height;
            const processContext = processCanvas.getContext('2d');

            // Draw the image onto the canvas
            processContext.drawImage(originalImage, 0, 0, originalImage.width, originalImage.height);

            // Get image data
            const processData = processContext.getImageData(0, 0, originalImage.width, originalImage.height);

            // Create grayscale image data
            const greyImageData = new Uint8Array(originalImage.width * originalImage.height);
            for (let i = 0; i < greyImageData.length; i++) {
                const offset = i * 4;
                greyImageData[i] = Math.floor((processData.data[offset] + processData.data[offset + 1] + processData.data[offset + 2]) / 3);
            }
            
            // Create target image object
            const targetImage = {
                data: greyImageData,
                height: originalImage.height,
                width: originalImage.width
            };
            
            
            
            // Processed image is available in targetImage
            console.log('Processed image:', targetImage);

            // Display the processed grayscale image
            const outputCanvas = document.createElement('canvas');
            outputCanvas.width = targetImage.width;
            outputCanvas.height = targetImage.height;
            const outputContext = outputCanvas.getContext('2d');
            const outputImageData = outputContext.createImageData(targetImage.width, targetImage.height);
            for (let i = 0; i < targetImage.data.length; i++) {
                const value = targetImage.data[i];
                outputImageData.data[i * 4] = value;     // R
                outputImageData.data[i * 4 + 1] = value; // G
                outputImageData.data[i * 4 + 2] = value; // B
                outputImageData.data[i * 4 + 3] = 255;   // A
            }
            outputContext.putImageData(outputImageData, 0, 0);
            document.body.appendChild(outputCanvas);

            const detector = new Detector(targetImage.width, targetImage.height);
            tf.nextFrame();
            const keyframe = tf.tidy(() => {
              const inputT = tf.tensor(targetImage.data, [targetImage.height, targetImage.width], 'float32');
              const { featurePoints: ps } = detector.detect(inputT);
              console.log("ps ", ps);
              desToTxT(ps);
              featurePointsToTxT(ps);
              
            });

            console.log('greyImageData', greyImageData);
            console.log('processData', processData);
            //const matchingData = _extractMatchingFeatures(imageList);
            //const trackingImageList = buildTrackingImageList(imageList);
            //console.log("matchingData ", matchingData);
            //console.log("trackingImageList ", trackingImageList);
            //const trackingData = extractTrackingFeatures(imageList);
            //console.log("trackingData ", trackingData);
            //showImage(trackingData[1]);
            
        }
    }
    reader.readAsDataURL(file);
}
}

function loadFASTFromFile(file, callback) {
  const reader = new FileReader();
  const points = [];

  reader.onload = function(e) {
      const text = e.target.result;
      const lines = text.split('\n');

      lines.forEach(line => {
          if (line.trim() === '') return;

          const [x, y] = line.split(',').map(num => parseFloat(num.trim()));
          if (!isNaN(x) && !isNaN(y)) {
              points.push({x: x, y: y});
          }
      });

      callback(points); // 使用回调返回 points
  };

  reader.onerror = () => callback(null, new Error('文件讀取錯誤'));
  reader.readAsText(file);
}


function testFASTTrackingAPI(files){
  if (files.length !== 3) {
    console.error('請選擇3張圖片');
    return;
  }
  console.log("testFASTTrackingAPI ");
  console.log("files 0 ", files[0]);
  console.log("files 1 ", files[1]);
  console.log("files 2 ", files[2]);
  loadFASTFromFile(files[1], (trackingPoints, error) => {
    if (error) {
        console.error(error);
    } else {
      console.log("trackingPoints ", trackingPoints);
      Promise.all([
        loadImage(files[0]),
        loadImage(files[2])
      ]).then(([{greyImageData: image1data, width: img1width, height: img1height}, {greyImageData: image2data, width: img2width, height: img2height}]) => {
        console.log("[1-image] image1 ", image1data, img1width, img1height);
        console.log("[1-image] image2 ", image2data, img2width, img2height);
        const Image1 = {
          data: image1data,
          height: img1height,
          width: img1width
        };
        const detector1 = new Detector(img1width, img1height);
        const Image2 = {
          data: image2data,
          height: img2height,
          width: img2width
        };
        const detector2 = new Detector(img2width, img2height);
    
        tf.nextFrame();
        const keyframes = [];
        const keyframe = tf.tidy(() => {
          // queue
          const inputT1 = tf.tensor(Image1.data, [img1height, img1width], 'float32');
          const {featurePoints: ps1} = detector1.detect(inputT1);
          targetInputHeight = img1height;
          targetInputWidth = img1width;
          imgScale = 1.0;
    
          // target
          const inputT2 = tf.tensor(Image2.data, [img2height, img2width], 'float32');
          const {featurePoints: ps2} = detector2.detect(inputT2);
          inputHeight = img2height;
          inputWidth = img2width;
    
          let bestH, bestMatchies;
          let matchingResult = ovoMatching(Image1, Image2, ps1, ps2);
          console.log("[0-detect] ps1 ", ps1, ps1.length);
          console.log("[0-detect] ps2 ", ps2, ps2.length);
          console.log("[0-matching] ovoMatchingFlag ", matchingResult.flag, matchingResult.bestH, matchingResult.bestMatchies);
    
          const screenCoords = [];
          const worldCoords = [];
          //const keyframe = keyframes[bestResult.keyframeIndex];
          for (let i = 0; i < matchingResult.bestMatchies.length; i++) {
            const querypoint = matchingResult.bestMatchies[i].querypoint;
            const keypoint = matchingResult.bestMatchies[i].keypoint;
            screenCoords.push({
              x: querypoint.x,
              y: querypoint.y,
            })
            worldCoords.push({
              x: (keypoint.x + 0.5) / imgScale,
              y: (keypoint.y + 0.5) / imgScale,
              z: 0,
            })
          }
    
          const fovy = 45.0 * Math.PI / 180; // 45 in radian. field of view vertical
          inputHeight = 720;
          inputWidth = 1280;
          const f = (inputHeight/2) / Math.tan(fovy/2);
          projectionTransform = [
            [f, 0, inputWidth / 2],
            [0, f, inputHeight / 2],
            [0, 0, 1]
            ];
    
          estimator = new Estimator(projectionTransform);
          const modelViewTransform = estimator.estimate({ screenCoords, worldCoords });
          
          const featuresSet = [];
          const featureSet = {
            data: Image1.data,
            scale: imgScale,
            width: targetInputWidth,
            height: targetInputHeight,
            points: trackingPoints,
          };
          featuresSet.push(featureSet);
          featuresSet.push(featureSet);
          const trackingDataList = [];
          const dimensions = [];
          for (let i = 0; i < 1; i++) {
            //matchingDataList.push(dataList[i].matchingData);
            trackingDataList.push(featuresSet);
            dimensions.push([targetInputWidth, targetInputHeight]);
          }
          console.log("[1-Estimator] modelViewTransform ", modelViewTransform, trackingDataList);
          console.log("[1-Tracker]  ", dimensions, trackingDataList);
          tracker = new Tracker(dimensions, trackingDataList, projectionTransform, targetInputWidth, targetInputHeight);
          console.log("[2-Tracker] inputT2 ", inputT2.dataSync());
          extractHToTxT(modelViewTransform);
          const result = tracker.track(inputT2, modelViewTransform, 0);
          console.log("[2-Tracker] result ", result);
          if (result.worldCoords.length < 4) return null;
          coordToTxt(result.screenCoords, result.worldCoords, "refineEstimateCoordsInput.txt");
          const finalModelViewTransform = estimator.refineEstimate({ initialModelViewTransform: modelViewTransform, worldCoords: result.worldCoords, screenCoords: result.screenCoords });
          console.log("[3-refineEstimate] track final ", projectionTransform, modelViewTransform, finalModelViewTransform);
        });
    });



    }
   });;
  

  
}

function testTrackingCase(files){
  if (files.length !== 2) {
    console.error('請選擇兩張圖片');
    return;
  }

  const file1 = files[0];
  const file2 = files[1];

  Promise.all([
    loadImage(file1),
    loadImage(file2)
  ]).then(([{greyImageData: image1data, width: img1width, height: img1height}, {greyImageData: image2data, width: img2width, height: img2height}]) => {
    console.log("[1-image] image1 ", image1data, img1width, img1height);
    console.log("[1-image] image2 ", image2data, img2width, img2height);
    const Image1 = {
      data: image1data,
      height: img1height,
      width: img1width
    };
    const detector1 = new Detector(img1width, img1height);
    const Image2 = {
      data: image2data,
      height: img2height,
      width: img2width
    };
    const detector2 = new Detector(img2width, img2height);

    tf.nextFrame();
    const keyframes = [];
    const keyframe = tf.tidy(() => {
      // queue
      const inputT1 = tf.tensor(Image1.data, [img1height, img1width], 'float32');
      const {featurePoints: ps1} = detector1.detect(inputT1);
      targetInputHeight = img1height;
      targetInputWidth = img1width;
      imgScale = 1.0;

      // target
      const inputT2 = tf.tensor(Image2.data, [img2height, img2width], 'float32');
      const {featurePoints: ps2} = detector2.detect(inputT2);
      inputHeight = img2height;
      inputWidth = img2width;

      console.log("[2-detect] ps1 ", ps1, ps1.length);
      console.log("[2-detect] ps2 ", ps2, ps2.length);
      const maximaPoints1 = ps1.filter((p) => p.maxima);
      const minimaPoints1 = ps1.filter((p) => !p.maxima);
      const maximaPoints2 = ps2.filter((p) => p.maxima);
      const minimaPoints2 = ps2.filter((p) => !p.maxima);
      const maximaPointsCluster1 = hierarchicalClusteringBuild({ points: maximaPoints1 });
      const minimaPointsCluster1 = hierarchicalClusteringBuild({ points: minimaPoints1 });
      const maximaPointsCluster2 = hierarchicalClusteringBuild({ points: maximaPoints2 });
      const minimaPointsCluster2 = hierarchicalClusteringBuild({ points: minimaPoints2 });
      console.log("[2-detect] ps1info ", maximaPoints1, minimaPoints1);
      console.log("[2-detect] ps2info ", maximaPoints2, minimaPoints2);
      let bestH, bestMatchies;
      let matchingResult = ovoMatching(Image1, Image2, ps1, ps2);
      console.log("[3-matching] ovoMatchingFlag ", matchingResult.flag, matchingResult.bestH, matchingResult.bestMatchies);

      const screenCoords = [];
      const worldCoords = [];
      //const keyframe = keyframes[bestResult.keyframeIndex];
      for (let i = 0; i < matchingResult.bestMatchies.length; i++) {
        const querypoint = matchingResult.bestMatchies[i].querypoint;
        const keypoint = matchingResult.bestMatchies[i].keypoint;
        screenCoords.push({
          x: querypoint.x,
          y: querypoint.y,
        })
        worldCoords.push({
          x: (keypoint.x + 0.5) / imgScale,
          y: (keypoint.y + 0.5) / imgScale,
          z: 0,
        })
      }
      console.log("[3-matching] Coord Result ", screenCoords, worldCoords);
      const near = 10;
      const far = 100000;
      const fovy = 45.0 * Math.PI / 180; // 45 in radian. field of view vertical
      const f = (inputHeight/2) / Math.tan(fovy/2);
      //     [fx  s cx]
      // K = [ 0 fx cy]
      //     [ 0  0  1]
      projectionTransform = [
        [f, 0, inputWidth / 2],
        [0, f, inputHeight / 2],
        [0, 0, 1]
      ];
      console.log("[4-Estimator] projectTransform", projectionTransform);
      estimator = new Estimator(projectionTransform);
      const modelViewTransform = estimator.estimate({ screenCoords, worldCoords });
      
      console.log("[4-Estimator] modelViewTransform ", modelViewTransform);
      
      
      const trackingPoints = extract({data: Image1.data, width: targetInputWidth, height: targetInputHeight, scale: imgScale});
      console.log("[5-extract] trackingPoints ", trackingPoints);
      const featuresSet = [];
      const featureSet = {
        data: Image1.data,
        scale: imgScale,
        width: targetInputWidth,
        height: targetInputHeight,
        points: trackingPoints,
      };
      featuresSet.push(featureSet);
      featuresSet.push(featureSet);
      const trackingDataList = [];
      const dimensions = [];
      for (let i = 0; i < 1; i++) {
        //matchingDataList.push(dataList[i].matchingData);
        trackingDataList.push(featuresSet);
        dimensions.push([targetInputWidth, targetInputHeight]);
      }
      
      console.log("[6-Tracker]  ", trackingDataList);
      console.log("[6-Tracker] projectionTransform ", projectionTransform, trackingDataList, projectionTransform, targetInputWidth, targetInputHeight);
      tracker = new Tracker(dimensions, trackingDataList, projectionTransform, targetInputWidth, targetInputHeight);
      console.log("[6-Tracker] inputT2 ", inputT2.dataSync());
      const result = tracker.track(inputT2, modelViewTransform, 0);
      console.log("[6-Tracker] result ", result);
      if (result.worldCoords.length < 4) return null;
      const finalModelViewTransform = estimator.refineEstimate({ initialModelViewTransform: modelViewTransform, worldCoords: result.worldCoords, screenCoords: result.screenCoords });
      console.log("[7-refineEstimate] track final ", finalModelViewTransform);
    })
  }).catch(error => {
    console.error('Error loading images:', error);
  });
}

function extractPointsToTxT(ps){
  //const data = "This is the content of the file.";

  // 創建 Blob 對象
  //const blob = new Blob([data], { type: 'text/plain' });
  const lines = ps.map(fp =>
    `${fp.x}, ${fp.y}`
  );
  const data = lines.join('\n');
  const blob = new Blob([data], { type: 'text/plain' });

  // 創建 URL 對象
  const url = URL.createObjectURL(blob);

  // 創建下載鏈接
  const a = document.createElement('a');
  a.href = url;
  a.download = 'extractPoints.txt'; // 文件名
  a.style.display = 'none'; // 隱藏鏈接
  document.body.appendChild(a);
  a.click();

  // 清理
  document.body.removeChild(a);
  URL.revokeObjectURL(url);
}

function extractHToTxT(H){
  //const data = "This is the content of the file.";

  // 創建 Blob 對象
  //const blob = new Blob([data], { type: 'text/plain' });
  let data;
  if (Array.isArray(H[0])) {
    // 如果是二維數組
    data = H.map(row => row.join(', ')).join('\n');
  } else {
    // 如果是一維數組
    data = H.join(', ');
  }
  const blob = new Blob([data], { type: 'text/plain' });

  // 創建 URL 對象
  const url = URL.createObjectURL(blob);

  // 創建下載鏈接
  const a = document.createElement('a');
  a.href = url;
  a.download = 'H.txt'; // 文件名
  a.style.display = 'none'; // 隱藏鏈接
  document.body.appendChild(a);
  a.click();

  // 清理
  document.body.removeChild(a);
  URL.revokeObjectURL(url);
}

function testTrackingPtsCase(file){
  if (file) {
    const reader = new FileReader();
    reader.onload = function (e) {
        console.log('reader onload');
        originalImage.src = e.target.result;

        originalImage.onload = function () {
            const processCanvas = document.createElement('canvas');
            processCanvas.width = originalImage.width;
            processCanvas.height = originalImage.height;
            const processContext = processCanvas.getContext('2d');

            // Draw the image onto the canvas
            processContext.drawImage(originalImage, 0, 0, originalImage.width, originalImage.height);

            // Get image data
            const processData = processContext.getImageData(0, 0, originalImage.width, originalImage.height);

            // Create grayscale image data
            const greyImageData = new Uint8Array(originalImage.width * originalImage.height);
            console.log("processData ", processData);
            for (let i = 0; i < greyImageData.length; i++) {
                const offset = i * 4;
                greyImageData[i] = Math.floor((processData.data[offset] + processData.data[offset + 1] + processData.data[offset + 2]) / 3);
            }
            console.log("greyImageData ", greyImageData, originalImage.width, originalImage.height);

            // Create target image object
            const targetImage = {
                data: greyImageData,
                height: originalImage.height,
                width: originalImage.width
            };

            // Processed image is available in targetImage
            console.log('Processed image:', targetImage);

            // Display the processed grayscale image
            const outputCanvas = document.createElement('canvas');
            outputCanvas.width = targetImage.width;
            outputCanvas.height = targetImage.height;
            const outputContext = outputCanvas.getContext('2d');
            const outputImageData = outputContext.createImageData(targetImage.width, targetImage.height);
            for (let i = 0; i < targetImage.data.length; i++) {
                const value = targetImage.data[i];
                outputImageData.data[i * 4] = value;     // R
                outputImageData.data[i * 4 + 1] = value; // G
                outputImageData.data[i * 4 + 2] = value; // B
                outputImageData.data[i * 4 + 3] = 255;   // A
            }
            outputContext.putImageData(outputImageData, 0, 0);
            document.body.appendChild(outputCanvas);
            //console.log("outputImageData ", outputImageData);
            //console.log("targetImage ", targetImage);
            //const imageList = buildImageList(targetImage);
            ///console.log("imageList ", imageList);
            //const matchingData = _extractMatchingFeatures(imageList);
            //const trackingImageList = buildTrackingImageList(imageList);
            //console.log("matchingData ", matchingData);
            //console.log("trackingImageList ", trackingImageList);
            //const trackingData = extractTrackingFeatures(imageList);
            //const image = imageList[0];
            //console.log("extractTrackingFeatures image ", image);
            const points = extract(targetImage);
            const featureSet = {
              data: targetImage.data,
              scale: 1,
              width: targetImage.width,
              height: targetImage.height,
              points,
            };
            console.log("featureSet ", featureSet);
            showImage(featureSet);
            console.log(points);
            extractPointsToTxT(points);
        }
    }
    reader.readAsDataURL(file);
}
}