//import './detector/binomialFilter.js';
//import * as tf from '@tensorflow/tfjs';
import {computeHoughMatches} from './matching/hough.js';
import './detector/lib/index.js'



// hough.js
function testComputeHoughMatches1(){
    const keywidth = 100, keyheight = 100, querywidth = 200, queryheight = 200;
    const matches = [
      {querypoint: {x: 10.0, y: 20.0, scale: 1.5, angle: 0.5}, keypoint: {x: 15.0, y: 25.0, scale: 1.2, angle: 0.6}},
      {querypoint: {x: 50.0, y: 60.0, scale: 2.0, angle: 1.0}, keypoint: {x: 55.0, y: 65.0, scale: 1.8, angle: 1.1}},
      {querypoint: {x: 100.0, y: 120.0, scale: 0.8, angle: -0.5}, keypoint: {x: 105.0, y: 125.0, scale: 0.9, angle: -0.4}},
      {querypoint: {x: 150.0, y: 180.0, scale: 1.2, angle: 2.0}, keypoint: {x: 155.0, y: 185.0, scale: 1.1, angle: 2.1}},
      {querypoint: {x: 80.0, y: 90.0, scale: 1.7, angle: -1.5}, keypoint: {x: 85.0, y: 95.0, scale: 1.6, angle: -1.4}}
    ];
    console.log("matches", matches);

    // Run JavaScript version
    const options = {keywidth, keyheight, querywidth, queryheight, matches};
    const jsResult = computeHoughMatches(options);
    console.log(jsResult);
  }

  function testComputeHoughMatches2(){
        const keywidth = 150, keyheight = 150, querywidth = 300, queryheight = 300;
        const matches = [
            {querypoint: {x: 25.0, y: 35.0, scale: 1.2, angle: 0.8}, keypoint: {x: 30.0, y: 40.0, scale: 1.1, angle: 0.9}},
            {querypoint: {x: 75.0, y: 85.0, scale: 1.8, angle: 1.2}, keypoint: {x: 80.0, y: 90.0, scale: 1.7, angle: 1.3}},
            {querypoint: {x: 120.0, y: 130.0, scale: 0.9, angle: -0.3}, keypoint: {x: 125.0, y: 135.0, scale: 1.0, angle: -0.2}},
            {querypoint: {x: 200.0, y: 220.0, scale: 1.5, angle: 1.8}, keypoint: {x: 205.0, y: 225.0, scale: 1.4, angle: 1.9}},
            {querypoint: {x: 100.0, y: 110.0, scale: 2.0, angle: -1.0}, keypoint: {x: 105.0, y: 115.0, scale: 1.9, angle: -0.9}},
            {querypoint: {x: 45.0, y: 55.0, scale: 1.3, angle: 0.5}, keypoint: {x: 50.0, y: 60.0, scale: 1.2, angle: 0.6}}
        ];
        console.log("matches", matches);
    
        // Run JavaScript version
        const options = {keywidth, keyheight, querywidth, queryheight, matches};
        const jsResult = computeHoughMatches(options);
        console.log(jsResult);
}
 

 function testComputeHoughMatches3(){
        const keywidth = 200, keyheight = 200, querywidth = 400, queryheight = 400;
        const matches = [
            {querypoint: {x: 30.0, y: 40.0, scale: 1.1, angle: 0.7}, keypoint: {x: 35.0, y: 45.0, scale: 1.0, angle: 0.8}},
            {querypoint: {x: 90.0, y: 100.0, scale: 1.9, angle: 1.3}, keypoint: {x: 95.0, y: 105.0, scale: 1.8, angle: 1.4}},
            {querypoint: {x: 150.0, y: 160.0, scale: 0.7, angle: -0.4}, keypoint: {x: 155.0, y: 165.0, scale: 0.8, angle: -0.3}},
            {querypoint: {x: 250.0, y: 270.0, scale: 1.6, angle: 2.0}, keypoint: {x: 255.0, y: 275.0, scale: 1.5, angle: 2.1}},
            {querypoint: {x: 120.0, y: 130.0, scale: 2.1, angle: -1.1}, keypoint: {x: 125.0, y: 135.0, scale: 2.0, angle: -1.0}},
            {querypoint: {x: 60.0, y: 70.0, scale: 1.4, angle: 0.6}, keypoint: {x: 65.0, y: 75.0, scale: 1.3, angle: 0.7}},
            {querypoint: {x: 180.0, y: 190.0, scale: 1.7, angle: 1.5}, keypoint: {x: 185.0, y: 195.0, scale: 1.6, angle: 1.6}}
        ];
        console.log("matches", matches);
    
        // Run JavaScript version
        const options = {keywidth, keyheight, querywidth, queryheight, matches};
        const jsResult = computeHoughMatches(options);
        console.log(jsResult);
}

function testComputeHoughMatches(){
    testComputeHoughMatches1();
    testComputeHoughMatches2();
    testComputeHoughMatches3();
}
 

  export {
    //testBinomialFilter,
    testComputeHoughMatches,
  }