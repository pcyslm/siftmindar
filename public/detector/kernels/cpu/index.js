//import { registerKernel } from '@tensorflow/tfjs';
import {registerKernel} from '@tensorflow/tfjs-core/dist/base';
import '@tensorflow/tfjs-core';
import * as tf from '@tensorflow/tfjs';
import { binomialFilterConfig } from './binomialFilter.js';
import { buildExtremasConfig } from './buildExtremas.js';
import { computeExtremaAnglesConfig } from './computeExtremaAngles.js';
import { computeExtremaFreakConfig } from './computeExtremaFreak.js';
import { computeFreakDescriptorConfig } from './computeFreakDescriptors.js';
import { computeLocalizationConfig } from './computeLocalization.js';
import { computeOrientationHistogramsConfig } from './computeOrientationHistograms.js';
import { downsampleBilinearConfig } from './downsampleBilinear.js';
import { extremaReductionConfig } from './extremaReduction.js';
import { smoothHistogramsConfig } from './smoothHistograms.js';
import { upsampleBilinearConfig } from './upsampleBilinear.js';


tf.registerKernel(binomialFilterConfig);
tf.registerKernel(buildExtremasConfig);
tf.registerKernel(computeExtremaAnglesConfig);
tf.registerKernel(computeExtremaFreakConfig);
tf.registerKernel(computeFreakDescriptorConfig);
tf.registerKernel(computeLocalizationConfig);
tf.registerKernel(computeOrientationHistogramsConfig);
tf.registerKernel(downsampleBilinearConfig);
tf.registerKernel(extremaReductionConfig);
tf.registerKernel(smoothHistogramsConfig);
tf.registerKernel(upsampleBilinearConfig);

