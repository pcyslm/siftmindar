import * as FakeShader from './fakeShader.js';
import { FREAKPOINTS } from "../freak.js";
const FREAK_EXPANSION_FACTOR = 7.0;

function getProgram(targetImage) {
    const kernel = {
        variableNames: ['p'],
        outputShape: [targetImage.shape[0], targetImage.shape[1]],
        userCode: function () {
            const coords = this.getOutputCoords();

            let sum = 0.0;
            sum += this.getP(coords[0]-1, coords[1]);
            this.setOutput(sum);
        }

    };
    return kernel;
}

function GetKernels(image) {
    const imageWidth = image.shape[1];
    const key = 'w' + imageWidth;
  
    const imageHeight = image.shape[0];
    const kernel1 = {
      variableNames: ['p'],
      outputShape: [imageHeight, imageWidth],
      userCode: function () {
        const coords = this.getOutputCoords();
  
        let sum = 0
        sum += this.getP(coords[0], coords[1] - 2);
        sum += this.getP(coords[0], coords[1] - 1) * 4.;
        sum += this.getP(coords[0], coords[1]) * 6.;
        sum += this.getP(coords[0], coords[1] + 1) * 4.;
        sum += this.getP(coords[0], coords[1] + 2);
        this.setOutput(sum);
      }
  
    };
    const kernel2 = {
      variableNames: ['p'],
      outputShape: [imageHeight, imageWidth],
      userCode: function () {
        const coords = this.getOutputCoords();
  
        let sum = this.getP(coords[0] - 2, coords[1]);
        sum += this.getP(coords[0] - 1, coords[1]) * 4.;
        sum += this.getP(coords[0], coords[1]) * 6.;
        sum += this.getP(coords[0] + 1, coords[1]) * 4.;
        sum += this.getP(coords[0] + 2, coords[1]);
        sum /= 256.;
        this.setOutput(sum);
      }
  
    };
    return [kernel1, kernel2];
    //}
  
  }

  function GetProgram(prunedExtremasHeight, pyramidImagesLength) {

    const imageVariableNames = [];
    for (let i = 1; i < pyramidImagesLength; i++) {
        imageVariableNames.push('image' + i);
    }


    const kernel = {
        variableNames: [...imageVariableNames, 'extrema', 'angles', 'freakPoints'],
        outputShape: [prunedExtremasHeight, FREAKPOINTS.length],
        userCode: function () {
            const getPixel=(octave, y, x)=> {
                const key = 'getImage' + octave;
                if (octave < 1 || octave >= pyramidImagesLength)
                    return 0.0;
                return this[key](y, x);
            }
            const coords = this.getOutputCoords();
            const featureIndex = coords[0];
            const freakIndex = coords[1];

            //const freakSigma = this.getFreakPoints(freakIndex, 0);
            const freakX = this.getFreakPoints(freakIndex, 1);
            const freakY = this.getFreakPoints(freakIndex, 2);

            const octave = this.int(this.getExtrema(featureIndex, 1));
            const inputY = this.getExtrema(featureIndex, 2);
            const inputX = this.getExtrema(featureIndex, 3);
            const inputAngle = this.getAngles(featureIndex);
            //const cos = FREAK_EXPANSION_FACTOR * Math.cos(inputAngle);
            //const sin = FREAK_EXPANSION_FACTOR * Math.sin(inputAngle);

            //const yp = inputY + freakX * sin + freakY * cos;
            //const xp = inputX + freakX * cos + freakY * -sin;

            //const x0 = this.int(Math.floor(xp));
            //const x1 = x0 + 1;
            //const y0 = this.int(Math.floor(yp));
            //const y1 = y0 + 1;

            //const f1 = getPixel(octave, y0, x0);
            //const f2 = getPixel(octave, y0, x1);
            //const f3 = getPixel(octave, y1, x0);
            //const f4 = getPixel(octave, y1, x1);

            /* const x1f = float(x1);
            const y1f = float(y1);
            const x0f = float(x0);
            const y0f = float(y0); */

            // ratio for interpolation between four neighbouring points
            //const value = (x1 - xp) * (y1 - yp) * f1
            //    + (xp - x0) * (y1 - yp) * f2
            //    + (x1 - xp) * (yp - y0) * f3
            //    + (xp - x0) * (yp - y0) * f4;

            this.setOutput(inputAngle);
        }

    }

    return kernel;

}

export const testShader = (args) => {
    
    /** @type {import('@tensorflow/tfjs').TensorInfo} */
    //const {targetImage } = args.inputs;
 
    /** @type {MathBackendCPU} */
    //const cpuBackend = args.backend;
    
    //const program = getProgram(targetImage);
    //return FakeShader.runCode(cpuBackend,program,[targetImage],targetImage.dtype);
    /*
    // @type {import('@tensorflow/tfjs').TensorInfo} 
    const image = args.inputs.image;
    // @type {MathBackendCPU} 
    const backend = args.backend;
  
    const [kernel1, kernel2] = GetKernels(image);
    console.log("kernel1 ", kernel1);
    console.log("kernel2 ", kernel2);
  
    const result1 = FakeShader.runCode(backend, kernel1, [image], image.dtype);
    const result2 = FakeShader.runCode(backend, kernel2, [result1], image.dtype);
    return result2;//
    */
    /** @type {import('@tensorflow/tfjs').TensorInfo} */
    const {gaussianImagesT, prunedExtremas, prunedExtremasAngles, freakPointsT, pyramidImagesLength } = args.inputs;
    /** @type {MathBackendCPU} */
    console.log("gaussianImagesT ", gaussianImagesT[0]);
    console.log("prunedExtremas ", prunedExtremas);
    prunedExtremas.print();
    console.log("prunedExtremasAngles ", prunedExtremasAngles);
    prunedExtremasAngles.print();
    console.log("freakPointsT ", freakPointsT);
    //freakPointsT.print();
    console.log("pyramidImagesLength ", pyramidImagesLength);
    const backend = args.backend;
    const prog = GetProgram(prunedExtremas.shape[0], pyramidImagesLength);
    return FakeShader.runCode(backend, prog, [...gaussianImagesT, prunedExtremas, prunedExtremasAngles, freakPointsT], 'float32');
}

export const testShaderConfig = {//: KernelConfig
    kernelName: "testShader",
    backendName: 'cpu',
    kernelFunc: testShader,// as {} as KernelFunc,
};