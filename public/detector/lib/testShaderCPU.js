import * as FakeShader from './fakeShader.js';
function getProgram(targetImage) {
    const kernel = {
        variableNames: ['p'],
        outputShape: [targetImage.shape[0], targetImage.shape[1]],
        userCode: function () {
            const coords = this.getOutputCoords();

            let sum = 0.0;
            sum += this.getP(coords[0]-1, coords[1]);
            this.setOutput(sum);
        }

    };
    return kernel;
}

export const testShaderCPU = (args) => {
    /** @type {import('@tensorflow/tfjs').TensorInfo} */
    const {targetImage } = args.inputs;
 
    /** @type {MathBackendCPU} */
    const cpuBackend = args.backend;
    
    const program = getProgram(targetImage);
    return FakeShader.runCode(cpuBackend,program,[targetImage],targetImage.dtype);

}

export const testShaderCPUConfig = {//: KernelConfig
    kernelName: "testShaderCPU",
    backendName: 'cpu',
    kernelFunc: testShaderCPU,// as {} as KernelFunc,
};