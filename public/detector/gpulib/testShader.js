
const cache={};


function GetProgram(targetImage) {
    const targetImageWidth = targetImage.shape[1];
    const targetImageHeight = targetImage.shape[0];
    const key = 'w' + targetImageWidth + "h" + targetImageHeight;
    if (!cache.hasOwnProperty(key)) {
      const kernel = {
        variableNames: ['p'],
        outputShape: [targetImageHeight, targetImageWidth],
        userCode: `
          void main() {
            ivec2 coords = getOutputCoords();
            setOutput(getP(coords[0]-1, coords[1]));
          }
        `
      };
      cache[key] = kernel;
    }
    
    return cache[key];
  }
  
  const testShader = (args) => {
    const {targetImage} = args.inputs;
    const backend = args.backend;
    console.log(targetImage);
    const program = GetProgram(targetImage);
    
    return backend.runWebGLProgram(program, [targetImage], targetImage.dtype);
  }
  
  export const testShaderConfig = {
    kernelName: "testShader",
    backendName: 'webgl',
    kernelFunc: testShader,
  };