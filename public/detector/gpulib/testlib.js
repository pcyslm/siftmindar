//import './index.js'
import * as tf from '@tensorflow/tfjs' 
import { FREAKPOINTS } from "../freak.js";


const PYRAMID_MIN_SIZE = 8;
const PYRAMID_MAX_OCTAVE = 5;

const LAPLACIAN_THRESHOLD = 3.0;
const LAPLACIAN_SQR_THRESHOLD = LAPLACIAN_THRESHOLD * LAPLACIAN_THRESHOLD;
const EDGE_THRESHOLD = 4.0;
const EDGE_HESSIAN_THRESHOLD =
  ((EDGE_THRESHOLD + 1) * (EDGE_THRESHOLD + 1)) / EDGE_THRESHOLD;

const NUM_BUCKETS_PER_DIMENSION = 10;
const MAX_FEATURES_PER_BUCKET = 5;
const NUM_BUCKETS = NUM_BUCKETS_PER_DIMENSION * NUM_BUCKETS_PER_DIMENSION;
// total max feature points = NUM_BUCKETS * MAX_FEATURES_PER_BUCKET

const ORIENTATION_NUM_BINS = 36;
const ORIENTATION_SMOOTHING_ITERATIONS = 5;

const ORIENTATION_GAUSSIAN_EXPANSION_FACTOR = 3.0;
const ORIENTATION_REGION_EXPANSION_FACTOR = 1.5;
const FREAK_EXPANSION_FACTOR = 7.0;

const FREAK_CONPARISON_COUNT =
  ((FREAKPOINTS.length - 1) * FREAKPOINTS.length) / 2; // 666
  
tf.setBackend('webgl');
function testBinomialFilter1(){
    const testImage = tf.tensor2d([
    [1, 2, 3, 4, 5],
    [6, 7, 8, 9, 10],
    [11, 12, 13, 14, 15],
    [16, 17, 18, 19, 20],
    [21, 22, 23, 24, 25]
   ]);
   console.log("testBinomialFilter1, ", testImage);
   const result = tf.engine().runKernel('BinomialFilter', { image:testImage });
  result.print();
  }

  function testBinomialFilter2(){
    const testImage = tf.tensor2d([
      [1, 2, 255, 4, 5],
      [6, 7, 8, 9, 6],
      [2, 12, 13, 14, 15],
      [244, 17, 18, 2, 4],
      [21, 7, 2, 24, 25]
     ]);
    //const result = this._applyFilter(testImage)
    const result = tf.engine().runKernel('BinomialFilter', { image:testImage });
    result.print();
  }

  function testBuildExtremas1() {
    // 創建測試輸入
    // 創建測試輸入
  const image0 = tf.tensor2d([
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1],
    [1, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 1],
    [1, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4, 3, 2, 1, 1],
    [1, 1, 2, 3, 4, 5, 5, 5, 5, 5, 4, 3, 2, 1, 1],
    [1, 1, 2, 3, 4, 5, 10, 10, 10, 5, 4, 3, 2, 1, 1],
    [1, 1, 2, 3, 4, 5, 10, 20, 10, 5, 4, 3, 2, 1, 1],
    [1, 1, 2, 3, 4, 5, 10, 10, 10, 5, 4, 3, 2, 1, 1],
    [1, 1, 2, 3, 4, 5, 5, 5, 5, 5, 4, 3, 2, 1, 1],
    [1, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4, 3, 2, 1, 1],
    [1, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 1],
    [1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  ]);
  
  const image1 = tf.tensor2d([
    [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
    [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
    [2, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2],
    [2, 2, 4, 6, 6, 6, 6, 6, 6, 6, 6, 6, 4, 2, 2],
    [2, 2, 4, 6, 8, 8, 8, 8, 8, 8, 8, 6, 4, 2, 2],
    [2, 2, 4, 6, 8, 10, 10, 10, 10, 10, 8, 6, 4, 2, 2],
    [2, 2, 4, 6, 8, 10, 20, 20, 20, 10, 8, 6, 4, 2, 2],
    [2, 2, 4, 6, 8, 10, 20, 40, 20, 10, 8, 6, 4, 2, 2],
    [2, 2, 4, 6, 8, 10, 20, 20, 20, 10, 8, 6, 4, 2, 2],
    [2, 2, 4, 6, 8, 10, 10, 10, 10, 10, 8, 6, 4, 2, 2],
    [2, 2, 4, 6, 8, 8, 8, 8, 8, 8, 8, 6, 4, 2, 2],
    [2, 2, 4, 6, 6, 6, 6, 6, 6, 6, 6, 6, 4, 2, 2],
    [2, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2],
    [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
    [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
  ]);
  
  const image2 = tf.tensor2d([
    [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
    [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
    [3, 3, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 3, 3],
    [3, 3, 6, 9, 9, 9, 9, 9, 9, 9, 9, 9, 6, 3, 3],
    [3, 3, 6, 9, 12, 12, 12, 12, 12, 12, 12, 9, 6, 3, 3],
    [3, 3, 6, 9, 12, 15, 15, 15, 15, 15, 12, 9, 6, 3, 3],
    [3, 3, 6, 9, 12, 15, 30, 30, 30, 15, 12, 9, 6, 3, 3],
    [3, 3, 6, 9, 12, 15, 30, 60, 30, 15, 12, 9, 6, 3, 3],
    [3, 3, 6, 9, 12, 15, 30, 30, 30, 15, 12, 9, 6, 3, 3],
    [3, 3, 6, 9, 12, 15, 15, 15, 15, 15, 12, 9, 6, 3, 3],
    [3, 3, 6, 9, 12, 12, 12, 12, 12, 12, 12, 9, 6, 3, 3],
    [3, 3, 6, 9, 9, 9, 9, 9, 9, 9, 9, 9, 6, 3, 3],
    [3, 3, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 3, 3],
    [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
    [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3]
    ]);
    console.log("testBuildExtremas1");
    // 執行buildExtremas
    //const result = tf.engine().runKernel(buildExtremas, {image0, image1, image2});
    let result = this._buildExtremas(image0, image1, image2);
    const data = result.arraySync();
    data.forEach((row, i) => {
      row.forEach((value, j) => {
          if (value !== 0) {
              console.log(`Position: [${i}, ${j}], Value: ${value}`);
          }
      });
  });
  console.log("testBuildExtremas1 end", result.shape);
  }

  function testBuildExtremas2() {
    
  }

  function testComputeExtremaAngles1(){
    const histogramsData = [
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 5, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 4, 6, 3, 1, 0, 0, 0, 0, 0, 0, 0]
    ];
    const inTensor = tf.tensor2d(histogramsData);
    
    const outputTensorT = tf.engine().runKernel('ComputeExtremaAngles', { histograms:  inTensor});
    outputTensorT.print();
  }

  function testComputeExtremaAngles2(){
    const histogramsData = [
      [0, 0, 0, 0, 1, 3, 5, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 4, 7, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ];
    const inTensor = tf.tensor2d(histogramsData);
    
    const outputTensorT = tf.engine().runKernel('ComputeExtremaAngles', { histograms:  inTensor});
    outputTensorT.print();
  }

  function testComputeLocatlization1(){
    const a = [
      [0.8, 1, 32, 48],
      [0.7, 2, 20, 30],
      [0.9, 1, 50, 60],
      [0.6, 3, 10, 15],
      [0.75, 2, 40, 45]
    ];

  // 設計 b 矩陣 (dogPyramidImagesT)
  const b = [];
  
 // 創建常數矩陣 (範圍 0-255)
 const input0T = tf.tensor([
  [50, 100, 150, 200, 250, 200, 150, 100],
  [75, 125, 175, 225, 225, 175, 125, 75],
  [100, 150, 200, 250, 200, 150, 100, 50],
  [125, 175, 225, 225, 175, 125, 75, 25],
  [150, 200, 250, 200, 150, 100, 50, 0],
  [175, 225, 225, 175, 125, 75, 25, 0],
  [200, 250, 200, 150, 100, 50, 0, 0],
  [225, 225, 175, 125, 75, 25, 0, 0]
], [8, 8], 'float32');

const input1T = tf.tensor([
  [50, 100, 150, 200],
  [100, 150, 200, 250],
  [150, 200, 250, 200],
  [200, 250, 200, 150]
], [4, 4], 'float32');

const input2T = tf.tensor([
  [100, 200],
  [200, 100]
], [2, 2], 'float32');

  b.push(input0T);
  b.push(input1T);
  b.push(input2T); // 模擬金字塔圖像
  //const outputTensorT = this._testComputeLocalization(a,b);
  const outputTensorT = tf.engine().runKernel('ComputeLocalization', {prunedExtremasList:a, dogPyramidImagesT:b,});
  

  const pixels = outputTensorT.arraySync();
  console.log("computerLocalization.js pixels start");
      for (let i = 0; i < pixels.length; i++) {
        for (let j = 0; j < pixels[i].length; j++) {
          for (let k = 0; k < pixels[i][j].length; k++) {
            if (pixels[i][j][k] !== 0) {
              console.log(`Non-zero value at [${i},${j},${k}]: ${pixels[i][j][k]}`);
            }
          }
        }
      }
    console.log("computerLocalization.js pixels end", pixels);
    outputTensorT.print();
  }

  function testComputeOrientationHistorgrams1(){
    const pyramidImagesLength = 3;
    const gaussianImagesT = [
          tf.tensor2d([
            [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0],
            [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 0.1],
            [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 0.1, 0.2],
            [0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 0.1, 0.2, 0.3],
            [0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 0.1, 0.2, 0.3, 0.4],
            [0.6, 0.7, 0.8, 0.9, 1.0, 0.1, 0.2, 0.3, 0.4, 0.5],
            [0.7, 0.8, 0.9, 1.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6],
            [0.8, 0.9, 1.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7],
            [0.9, 1.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8],
            [1.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
          ]),
          tf.tensor2d([
            [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1],
            [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 0.2],
            [0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 0.2, 0.3],
            [0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 0.2, 0.3, 0.4],
            [0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 0.2, 0.3, 0.4, 0.5],
            [0.7, 0.8, 0.9, 1.0, 1.1, 0.2, 0.3, 0.4, 0.5, 0.6],
            [0.8, 0.9, 1.0, 1.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7],
            [0.9, 1.0, 1.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8],
            [1.0, 1.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9],
            [1.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
          ])
        ];
        
        const prunedExtremasT = tf.tensor2d([
          [0, 1, 5, 5],
          [0, 2, 3, 7],
          [0, 1, 8, 2],
          [0, 2, 1, 6],
          [0, 1, 4, 4]
        ]);
        
        const radialPropertiesT = tf.tensor2d([
          [-1, 0, 0.5],
          [0, 1, 0.7],
          [1, -1, 0.3],
          [-1, 1, 0.6],
          [0, -1, 0.4],
          [1, 0, 0.8],
          [-1, -1, 0.2],
          [1, 1, 0.9]
        ]);
      
      const result = tf.engine().runKernel('ComputeOrientationHistograms', {gaussianImagesT,prunedExtremasT,radialPropertiesT,pyramidImagesLength: pyramidImagesLength,});

      console.log("Result shape:", result.shape);
      const data = result.arraySync();
      data.forEach((row, i) => {
        row.forEach((value, j) => {
            if (value !== 0) {
                console.log(`Position: [${i}, ${j}], Value: ${value}`);
            }
        });
    });;
      console.log("Result end");
  }

  function testExtremaReduction1(){
  const inputData = [
    [0,  0,  5,  0],
    [0, -3,  0,  0],
    [2,  0,  0,  4],
    [0,  1,  0,  0]
  ];
  const inputTensor = tf.tensor2d(inputData);
  const result = tf
    .engine()
    .runKernel('ExtremaReduction', {
      extremasResultT: inputTensor,
    });
    result.print();
}

function testDownsampleBilinear(){
  const testImage = tf.tensor2d([
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12],
    [13, 14, 15, 16]
  ]);
  
  console.log('Original Image:');
  testImage.print();
  
  // 使用 resizeBilinear 進行下採樣
  const result = tf.engine().runKernel('DownsampleBilinear', { image:testImage })
  console.log('Downsampled Image:');
  result.print();
}

function testUpsampleBilinear1(){
    const input1T = tf.tensor2d([
        [1, 2],
        [3, 4]
      ]);
    
      const input2T = tf.tensor2d([
        [1, 2, 3, 4, 8, 8],
        [3, 4, 5, 6, 8, 8],
        [7, 8, 9, 10, 8, 8],
        [11, 12, 13, 14, 8, 8]
      ]);

      // 使用 resizeBilinear 進行上採樣
      const upsampledImage = tf.engine().runKernel('UpsampleBilinear', { image:input1T, targetImage: input2T});
      
      console.log('Upsampled Image:');
      upsampledImage.print();  
}

function testUpsampleBilinear2(){
  const input1T = tf.tensor2d([
      [3, 4, 5],
      [1, 3, 10]
    ]);
  
    const input2T = tf.tensor2d([
      [3, 2, 3, 4, 8, 8],
      [3, 4, 5, 6, 8, 8],
      [7, 8, 9, 10, 8, 8],
      [11, 12, 13, 14, 8, 8]
    ]);

    // 使用 resizeBilinear 進行上採樣
    const upsampledImage = tf.engine().runKernel('UpsampleBilinear', { image:input1T, targetImage: input2T});
    
    console.log('Upsampled Image:');
    upsampledImage.print();  
}

function testComputeExtremaFreak(){
  const gaussianImagesT = [
    tf.tensor2d([[1, 2], [3, 4]]),
    tf.tensor2d([[5, 6], [7, 8]]),
  ];
  const prunedExtremas = tf.tensor2d([[0, 1, 0.5, 0.5]], [1, 4]);
  const prunedExtremasAngles = tf.tensor1d([Math.PI / 4]);
  const freakPointsT = tf.tensor2d(FREAKPOINTS);

  const result = tf.engine().runKernel('ComputeExtremaFreak', {
          gaussianImagesT: gaussianImagesT,
          prunedExtremas: prunedExtremas,
          prunedExtremasAngles: prunedExtremasAngles,
          freakPointsT: freakPointsT,
          pyramidImagesLength: gaussianImagesT.length,
        });
  
  console.log('testComputeExtremaFreak Result:');
  result.print();
}

function testShader1(){
  const input1T = tf.tensor2d([
    [1, 2, 3],
    [4, 5, 4],
  ]);

  const result = tf.engine().runKernel('testShader', {targetImage:input1T});
  console.log('testShader Result:');
  result.print();
}

function testShader2(){
  const input1T = tf.tensor2d([
    [1, 2],
    [3, 4],
  ]);

  const result = tf.engine().runKernel('testShader', {targetImage:input1T});
  console.log('testShader Result:');
  result.print();
}

function testShader3(){
  const input1T = tf.tensor2d([
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
  ]);

  const result = tf.engine().runKernel('testShader', {targetImage:input1T});
  console.log('testShader Result:');
  result.print();
}

function testShader4(){
  const input1T = tf.tensor2d([
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12],
    [13, 14, 15, 16],
  ]);

  const result = tf.engine().runKernel('testShader', {targetImage:input1T});
  console.log('testShader Result:');
  result.print();
}

function testSmoothHistograms(){
  const inputT = tf.tensor([
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 
     19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36],
    [36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 
     18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
     1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
   ]);
   const result = tf.engine().runKernel('SmoothHistograms', {histograms: inputT});
   console.log('testSmoothHistograms Result:');
   result.print();
}

function flatten(input, max) {
  return input.reduce((prev, current, index) => {
      for (let i = index + 1; i < max.length; i++) {
          current *= max[i];
      }
      return prev + current;
  },0);
}

function testFlatten(){
  const testCases = [
    { input: [0, 0], max: [2, 2], expected: 0 },
    { input: [0, 1], max: [2, 2], expected: 1 },
    { input: [1, 0], max: [2, 2], expected: 2 },
    { input: [1, 1], max: [2, 2], expected: 3 }
];

testCases.forEach(({ input, max, expected }) => {
    const result = flatten(input, max);
    console.log(`Input: [${input}], Max: [${max}], Expected: ${expected}, Result: ${result}`);
});
}

function testFunction(){
    //testBuildExtremas1();
    //testBuildExtremas2();
    //testComputeExtremaAngles1();
    //testComputeExtremaAngles2();
    //testComputeLocatlization1();
    //testComputeOrientationHistorgrams1();
    //testExtremaReduction1();
    //testDownsampleBilinear1();
    //testUpsampleBilinear1();
    //testUpsampleBilinear2();
    //testComputeExtremaFreak();
    
    //detectTest(inputT, width, height);
    //testSmoothHistograms();
    //testShader1();
    //testShader2();
    
    //testFlatten();
    testShader2();
    testShader4();
    testBinomialFilter1();
    testBinomialFilter2();
  }

  export {
    //testBinomialFilter,
    testFunction,
  }