//import { registerKernel } from '@tensorflow/tfjs';
//import {registerKernel} from '@tensorflow/tfjs-core/dist/base';
import '@tensorflow/tfjs-core';
import * as tf from '@tensorflow/tfjs';
import { computeProjectionConfig } from './computeProjection.js';
import { computeMatchingConfig, computeMatchingSimConfig,computeMatchingPointsConfig  } from './computeMatching.js';

tf.registerKernel(computeProjectionConfig);
tf.registerKernel(computeMatchingConfig);
tf.registerKernel(computeMatchingPointsConfig);
tf.registerKernel(computeMatchingSimConfig);