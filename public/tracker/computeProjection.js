import * as FakeShader from '../detector/cpulib/fakeShader';

const PRECISION_ADJUST = 1000;

function GetKernel(markerWidth, markerHeight, markerScale) {
  const kernel = {
    variableNames: ['M', 'pixel'],
    outputShape: [markerHeight, markerWidth],
    userCode: function() {
      const coords = this.getOutputCoords();

      const m00 = this.getM(0, 0) * PRECISION_ADJUST;
      const m01 = this.getM(0, 1) * PRECISION_ADJUST;
      const m03 = this.getM(0, 3) * PRECISION_ADJUST;
      const m10 = this.getM(1, 0) * PRECISION_ADJUST;
      const m11 = this.getM(1, 1) * PRECISION_ADJUST;
      const m13 = this.getM(1, 3) * PRECISION_ADJUST;
      const m20 = this.getM(2, 0) * PRECISION_ADJUST;
      const m21 = this.getM(2, 1) * PRECISION_ADJUST;
      const m23 = this.getM(2, 3) * PRECISION_ADJUST;

      const y = coords[0] / markerScale;
      const x = coords[1] / markerScale;
      const uz = (x * m20) + (y * m21) + m23;
      const oneOverUz = 1 / uz;

      let ux = (x * m00) + (y * m01) + m03;
      let uy = (x * m10) + (y * m11) + m13;

      ux = Math.floor(ux * oneOverUz + 0.5+1e-10); // boundary issue +1e-10
      uy = Math.floor(uy * oneOverUz + 0.5+1e-10); // boundary issue +1e-10


      this.setOutput(this.getPixel(uy, ux)); //
    }
  };

  return kernel;
}

export function _computeProjection(args) {
  const {trackingKeyframeList, modelViewProjectionTransformT, inputImageT, targetIndex} = args.inputs;
  const backend = args.backend;
  //console.log("_computeProjection ", trackingKeyframeList);
  const markerWidth = trackingKeyframeList[targetIndex].width;
  const markerHeight = trackingKeyframeList[targetIndex].height;
  const markerScale = trackingKeyframeList[targetIndex].scale;
  console.log("computeProjection Width Height out ", markerHeight, markerWidth, markerScale);
  //const kernelKey = `${markerWidth}-${markerHeight}-${markerScale}`;

  //if (!this.kernelCaches.computeProjection) {
  //  this.kernelCaches.computeProjection = {};
  //}
  //this.kernelCaches.computeProjection = {};
  //this.kernelCaches.computeProjection[kernelKey] = GetKernel(markerWidth, markerHeight, markerScale);
  //if (!this.kernelCaches.computeProjection[kernelKey]) {
  //  this.kernelCaches.computeProjection[kernelKey] = GetKernel(markerWidth, markerHeight, markerScale);
  //}
  
  const program = GetKernel(markerWidth, markerHeight, markerScale);//this.kernelCaches.computeProjection[kernelKey];//GetKernel(markerWidth, markerHeight, markerScale);
  const result = FakeShader.runCode(backend, program, [modelViewProjectionTransformT, inputImageT], inputImageT.dtype);
  
  return result;
  //return tf.tidy(() => {
    //const program = this.kernelCaches.computeProjection[kernelKey];
  //});
}

export const computeProjectionConfig = {
  kernelName: "ComputeProjection",
  backendName: 'cpu',
  kernelFunc: _computeProjection,
};