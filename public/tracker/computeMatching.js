import * as FakeShader from '../detector/cpulib/fakeShader';
import * as tf from '@tensorflow/tfjs';

const AR2_DEFAULT_TS = 6;
const AR2_DEFAULT_TS_GAP = 1;
const AR2_SEARCH_SIZE = 10;
const AR2_SEARCH_GAP = 1;
//const AR2_SIM_THRESH = 0.8;

function GetKernels(featureCount, targetWidth, targetHeight) {
  const templateOneSize = AR2_DEFAULT_TS;
  const templateSize = templateOneSize * 2 + 1;
  const templateGap = AR2_DEFAULT_TS_GAP;
  const searchOneSize = AR2_SEARCH_SIZE * templateGap;
  const searchGap = AR2_SEARCH_GAP;
  const searchSize = searchOneSize * 2 + 1;
  console.log("GetKernels searchSize ", searchSize);
  console.log("GetKernels targetWidth ", targetWidth);

  const kernel1 = {
    variableNames: ['features', 'markerPixels', 'markerProperties', 'targetPixels'],
    outputShape: [featureCount, searchSize * searchSize],
    userCode: function() {
      const coords = this.getOutputCoords();

      const featureIndex = coords[0];
      const searchOffsetIndex = coords[1];
      
      const markerWidth = Math.floor(this.getMarkerProperties(0));
      const markerHeight = Math.floor(this.getMarkerProperties(1));
      const markerScale = this.getMarkerProperties(2);

      const searchOffsetX = (searchOffsetIndex % searchSize) * searchGap;
      const searchOffsetY = Math.floor(searchOffsetIndex / searchSize) * searchGap;

      const sCenterX = Math.floor(this.getFeatures(featureIndex, 0) * markerScale);
      const sCenterY = Math.floor(this.getFeatures(featureIndex, 1) * markerScale);

      const sx = sCenterX + searchOffsetX - searchOneSize;
      const sy = sCenterY + searchOffsetY - searchOneSize;
      
      if (sx < templateOneSize || sx >= (targetWidth - templateOneSize) || sy < templateOneSize || sy >= (targetHeight - templateOneSize)) {
        this.setOutput(-2);
      } 
      else {
        let sumPoint = 0;
        let sumPointSquare = 0;
        let sumTemplate = 0;
        let sumTemplateSquare = 0;
        let sumPointTemplate = 0;

        for (let templateOffsetY = 0; templateOffsetY < templateSize; templateOffsetY++) {
          for (let templateOffsetX = 0; templateOffsetX < templateSize; templateOffsetX++) {
            const fx2 = sCenterX + templateOffsetX - templateOneSize;
            const fy2 = sCenterY + templateOffsetY - templateOneSize;

            const sx2 = sx + templateOffsetX - templateOneSize;
            const sy2 = sy + templateOffsetY - templateOneSize;

            const markerPixelIndex = fy2 * markerWidth + fx2;
            const markerPixel = this.getMarkerPixels(markerPixelIndex);
            const targetPixel = this.getTargetPixels(sy2, sx2);

            sumTemplate += markerPixel;
            sumTemplateSquare += markerPixel * markerPixel;
            sumPoint += targetPixel;
            sumPointSquare += targetPixel * targetPixel;
            sumPointTemplate += targetPixel * markerPixel;
          }
        }

        const count = templateSize * templateSize;
        const pointVariance = Math.sqrt(sumPointSquare - sumPoint / count * sumPoint);
        const templateVariance = Math.sqrt(sumTemplateSquare - sumTemplate / count * sumTemplate);

        if (pointVariance < 0.0000001) {
          this.setOutput(-3);
        } else if (templateVariance < 0.0000001) {
          this.setOutput(-4);
        } else {
          sumPointTemplate -= sumPoint / count * sumTemplate;
          const sim = sumPointTemplate / pointVariance / templateVariance;  
          this.setOutput(sim);
        }
      }
    
    }
  };

  const kernel2 = {
    variableNames: ['featurePoints', 'markerProperties', 'maxIndex'],
    outputShape: [featureCount, 2], // [x, y]
    userCode: function() {
      const coords = this.getOutputCoords();

      const markerScale = this.getMarkerProperties(2);

      const featureIndex = coords[0];

      const maxIndex = this.getMaxIndex(featureIndex);
      const searchLocationIndex = maxIndex / (searchSize * searchSize);
      const searchOffsetIndex = maxIndex % (searchSize * searchSize);

      if (coords[1] === 0) {
        const searchOffsetX = (searchOffsetIndex % searchSize) * searchGap;
        this.setOutput(this.getFeaturePoints(featureIndex, 0) + (searchOffsetX - searchOneSize) / markerScale);
      }
      else if (coords[1] === 1) {
        const searchOffsetY = (searchOffsetIndex / searchSize) * searchGap;
        this.setOutput(this.getFeaturePoints(featureIndex, 1) + (searchOffsetY - searchOneSize) / markerScale);
      }
    }
  };

  const kernel3 = {
    variableNames: ['sims', 'maxIndex'],
    outputShape: [featureCount],
    userCode: function() {
      const featureIndex = this.getOutputCoords();
      const maxIndex = this.getMaxIndex(featureIndex);
      this.setOutput(this.getSims(featureIndex, maxIndex));
    }
  };

  return [kernel1, kernel2, kernel3];
}

export function _computeMatching(args) {
  const {featurePointsT, imagePixelsT, imagePropertiesT, projectedImageT} = args.inputs;
  const targetHeight = projectedImageT.shape[0];
  const targetWidth = projectedImageT.shape[1];
  const featureCount = featurePointsT.shape[0];
  const backend = args.backend;
  console.log("_computeMatching ", targetWidth, targetHeight);
  //if (!this.kernelCaches.computeMatching) {
  //  this.kernelCaches.computeMatching = 
  //}

  //return tf.tidy(() => {
    const programs = GetKernels(featureCount, targetWidth, targetHeight);//this.kernelCaches.computeMatching;
    console.log("end 1, featurePointsT ", featurePointsT.dataSync);
    const allSims = FakeShader.runCode(backend, programs[0], [featurePointsT, imagePixelsT, imagePropertiesT, projectedImageT], 'float32');
    const maxIndex = allSims.argMax(1);
    
    const matchingPointsT = FakeShader.runCode(backend, programs[1], [featurePointsT, imagePropertiesT, maxIndex], 'float32');
    console.log("end 3, matchingPointsT", matchingPointsT.dataSync);
    const simT = FakeShader.runCode(backend, programs[2], [allSims, maxIndex], 'float32');
    console.log("end 4", matchingPointsT, simT);
    matchingPoints = matchingPointsT.dataSync();
    sim = simT.dataSync();
    console.log("matchingPoints sim", matchingPoints, sim);
    
    return matchingPoints;
  //});
}

export function _computeMatchingPoints(args) {
    const {featurePointsT, imagePixelsT, imagePropertiesT, projectedImageT} = args.inputs;
    const targetHeight = projectedImageT.shape[0];
    const targetWidth = projectedImageT.shape[1];
    const featureCount = featurePointsT.shape[0];
    const backend = args.backend;
    
    console.log("end 1, featurePointsT ", featurePointsT.dataSync());
    const programs = GetKernels(featureCount, targetWidth, targetHeight);//this.kernelCaches.computeMatching;
    console.log("_computeMatchingPoints featurePointsT, ", featurePointsT.dataSync());
    console.log("_computeMatchingPoints imagePixelsT, ", imagePixelsT.dataSync());
    console.log("_computeMatchingPoints imagePropertiesT, ", imagePropertiesT.dataSync());
    console.log("_computeMatchingPoints projectedImageT, ", projectedImageT.dataSync());
    const allSims = FakeShader.runCode(backend, programs[0], [featurePointsT, imagePixelsT, imagePropertiesT, projectedImageT], 'float32');
    console.log("end imagePixelsT, ", imagePixelsT.dataSync());
    console.log("end 2, ", featureCount, targetWidth, targetHeight, allSims.dataSync());
    const maxIndex = allSims.argMax(1);
    console.log("maxIndex ", maxIndex.dataSync());
    const matchingPointsT = FakeShader.runCode(backend, programs[1], [featurePointsT, imagePropertiesT, maxIndex], 'float32');
    
    console.log("end 3, matchingPointsT", matchingPointsT.dataSync);
    return matchingPointsT;
  }

  export function _computeMatchingSim(args) {
    const {featurePointsT, imagePixelsT, imagePropertiesT, projectedImageT} = args.inputs;
    const targetHeight = projectedImageT.shape[0];
    const targetWidth = projectedImageT.shape[1];
    const featureCount = featurePointsT.shape[0];
    const backend = args.backend;

    const programs = GetKernels(featureCount, targetWidth, targetHeight);//this.kernelCaches.computeMatching;
    const allSims = FakeShader.runCode(backend, programs[0], [featurePointsT, imagePixelsT, imagePropertiesT, projectedImageT], 'float32');
    console.log("allSims ", allSims.dataSync());
    const maxIndex = allSims.argMax(1);
    //const matchingPointsT = FakeShader.runCode(backend, programs[1], [featurePointsT, imagePropertiesT, maxIndex], 'float32');
    const simT = FakeShader.runCode(backend, programs[2], [allSims, maxIndex], 'float32');
    return simT;
  }

export const computeMatchingPointsConfig = {
  kernelName: "ComputeMatchingPoints",
  backendName: 'cpu',
  kernelFunc: _computeMatchingPoints,
};

export const computeMatchingConfig = {
    kernelName: "ComputeMatching",
    backendName: 'cpu',
    kernelFunc: _computeMatching,
};

export const computeMatchingSimConfig = {
    kernelName: "ComputeMatchingSim",
    backendName: 'cpu',
    kernelFunc: _computeMatchingSim,
};